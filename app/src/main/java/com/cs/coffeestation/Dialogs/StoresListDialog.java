package com.cs.coffeestation.Dialogs;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.cs.coffeestation.Adapters.StoreDialogAdapter;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.Brands;
import com.cs.coffeestation.R;

import java.util.ArrayList;
import java.util.Collections;

import static com.cs.coffeestation.Adapters.SearchmenuListAdapter.TAG;

public class StoresListDialog extends BottomSheetDialogFragment  {

    TextView storename,summarytext ;
    ImageView storelogo;
    View rootView;;
    RecyclerView listView;
    private StoreDialogAdapter mstorediolgadapter;
    private ShimmerRecyclerView Storelist;
    private ArrayList<Brands> mStorelist = new ArrayList<>();
    int pos = 0;

    public static StoresListDialog newInstance() {
        return new StoresListDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.storesview_dialog, container, false);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        mStorelist = (ArrayList<Brands>) getArguments().getSerializable("array");
        pos = getArguments().getInt("pos", 0);

        storename = (TextView) rootView.findViewById(R.id.storename);
        listView = (RecyclerView)rootView.findViewById(R.id.stores_list_dialog) ;
        summarytext = (TextView) rootView.findViewById(R.id.subtext);
        storelogo =(ImageView)rootView.findViewById(R.id.storelogo);

        storename.setText(mStorelist.get(pos).getBradNameEn());
        summarytext.setText(mStorelist.get(pos).getBrands().size()+" outlets near by you");

        Glide.with(getContext())
                .load(Constants.IMAGE_URL+mStorelist.get(pos).getBrandimageEn())
                .into(storelogo);

        Collections.sort(mStorelist.get(pos).getBrands(), Brands.distanceSort);
        mstorediolgadapter = new StoreDialogAdapter(getContext(), mStorelist, pos, getActivity());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        listView.setLayoutManager(layoutManager);
        listView.addItemDecoration(new DividerItemDecoration(listView.getContext(), DividerItemDecoration.HORIZONTAL));
        listView.setAdapter(mstorediolgadapter);
        return rootView;
    }
    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }
    private void setTypeface(){
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                "helvetica.ttf");
        storename.setTypeface(typeface);
        summarytext.setTypeface(typeface);
    }
    }

