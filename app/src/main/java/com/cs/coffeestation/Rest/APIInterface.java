package com.cs.coffeestation.Rest;

import com.cs.coffeestation.Models.AddAddress;
import com.cs.coffeestation.Models.DisplayProfile;
import com.cs.coffeestation.Models.MenuItems;
import com.cs.coffeestation.Models.MyAddress;
import com.cs.coffeestation.Models.StoresList;
import com.cs.coffeestation.Models.UserRegistrationResponse;
import com.cs.coffeestation.Models.VerifyMobileResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIInterface {

    @POST("UserAPI/GetMobileEmailVerify")
    Call<VerifyMobileResponse> verfiyMobileNumber(@Body RequestBody body);

    @POST("UserAPI/SaveUserDetails")
    Call<UserRegistrationResponse> userRegistration(@Body RequestBody body);

    @POST("UserAPI/GetUserValidation")
    Call<UserRegistrationResponse> userLogin(@Body RequestBody body);

    @POST("UserAPI/GetMobileVerify")
    Call<VerifyMobileResponse> forgotPassword(@Body RequestBody body);

    @POST("UserAPI/SetNewPassword")
    Call<UserRegistrationResponse> resetPassword(@Body RequestBody body);

    @POST("UserAuth/DisplayProfile")
    Call<DisplayProfile> displayProfile(@Body RequestBody body);

    @POST("UserAuth/UpdateProfile")
    Call<DisplayProfile> editProfile(@Body RequestBody body);

    @POST("UserAPI/ChangePassword")
    Call<UserRegistrationResponse> changePassword(@Body RequestBody body);

    @POST("UserAPI/GetUserAddress")
    Call<MyAddress> myAddress(@Body RequestBody body);

    @POST("UserAPI/SaveUserAddress")
    Call<AddAddress> addAddress(@Body RequestBody body);

    @POST("StoreInformationAPI/GetStoreCatInformation")
    Call<StoresList> getStoresList(@Body RequestBody body);

    @POST("MenuItemsAPI/GetCatgoriesWithItems")
    Call<MenuItems> getMenuItems(@Body RequestBody body);
 }
