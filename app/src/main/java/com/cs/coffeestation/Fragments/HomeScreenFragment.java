package com.cs.coffeestation.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.cs.coffeestation.Activities.ChangeAddressActivity;
import com.cs.coffeestation.Activities.MyAddressActivity;
import com.cs.coffeestation.Activities.StoreMenuActivity;
import com.cs.coffeestation.Activities.ViewallScreenActivity;
import com.cs.coffeestation.Adapters.Around3kmStoresAdapter;
import com.cs.coffeestation.Adapters.BannersAdapter;
import com.cs.coffeestation.Adapters.HomeLookingForStoresAdapter;
import com.cs.coffeestation.Adapters.MyAddressAdapter;
import com.cs.coffeestation.Adapters.NewStoresAdapter;
import com.cs.coffeestation.Adapters.PopularStoresAdapter;
import com.cs.coffeestation.Adapters.PromotedStoresAdapter;
import com.cs.coffeestation.Adapters.StoresAdapter;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Dialogs.SearchListDialog;
import com.cs.coffeestation.Models.Brands;
import com.cs.coffeestation.Models.MyAddress;
import com.cs.coffeestation.Models.StoresList;
import com.cs.coffeestation.R;
import com.cs.coffeestation.Rest.APIInterface;
import com.cs.coffeestation.Rest.ApiClient;
import com.cs.coffeestation.Utils.FlipAnimation;
import com.cs.coffeestation.Utils.GPSTracker;
import com.cs.coffeestation.Utils.NetworkUtil;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import io.supercharge.shimmerlayout.ShimmerLayout;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;

public class HomeScreenFragment extends Fragment implements View.OnClickListener, OnMapReadyCallback {

    private TextView localityTitle,viewall,viewall1;
    public static Double currentLatitude, currentLongitude;
    private ShimmerRecyclerView storesListView,bannerListView;
    private ViewPager promotedViewPager;
    private ViewPager popuplarViewPager;
    private ViewPager nearbyViewPager;
    private ViewPager newStoresViewPager;
    private ViewPager bannersViewpage;
    private ViewPager lookingStoresViewPager;
    private ShimmerLayout shimmerPromotedLayout;
    private ShimmerLayout shimmerPopularLayout;
    private ShimmerLayout shimmerNearbyLayout;
    private ShimmerLayout shimmerNewStoresLayout;
    private ShimmerLayout shimmerSomthingLayout;
    private StoresAdapter mStoreAdapter;
    private PromotedStoresAdapter mPromotedAdapter;
    private PopularStoresAdapter mPopularAdapter;
    private Around3kmStoresAdapter mAround;
    private NewStoresAdapter mNewAdapter;
    private HomeLookingForStoresAdapter mLookingAdapter;
    private BannersAdapter mBanners;
    private LinearLayout locationLayout;
    LinearLayout promotedlayout,populerlayout,newlayout,aroundlayout,bannerlayout;
    RelativeLayout lookigsomthinglayout;
    private RelativeLayout typesLayout;
    CircleIndicator defaultIndicator;
    ACProgressFlower dialog;
    SharedPreferences userPrefs;
    public static ArrayList<StoresList.BannerResult> bannerList = new ArrayList<>();
    public static ArrayList<StoresList.StoresDetails> storesList = new ArrayList<>();
    public static ArrayList<StoresList.StoresDetails> popularList = new ArrayList<>();
    public static ArrayList<StoresList.StoresDetails> itsnewList = new ArrayList<>();
    public static ArrayList<StoresList.StoresDetails> promotedList = new ArrayList<>();
    public static ArrayList<StoresList.StoresDetails> threekmList = new ArrayList<>();
    public static ArrayList<StoresList.StoreCategories> somthingList = new ArrayList<>();

    ArrayList<Brands> brandslist = new ArrayList<>();
    ArrayList<Integer> seletedbrands = new ArrayList<>();
    GPSTracker gps;
    String TAG = "TAGF";
    private static final String[] LOCATION_PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 1;
    private static final int ADDRESS_REQUEST = 2;
    private Context context;
    public static boolean isCurrentLocationSelected = true;
    private TextView popularText, nearbyText, newStoresText,lookingsomthingTex;
    private TextView lookingText, offertext1, offertext2, shareText1, shareText2;
    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private LinearLayout mapLayout;
    private NestedScrollView listLayout;
    private ImageView mapIcon;
    public  static Double latitude, longitude;
    private FusedLocationProviderClient mFusedLocationClient;

    View rootView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home_screen, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        localityTitle = (TextView) rootView.findViewById(R.id.home_location);
        popularText = (TextView) rootView.findViewById(R.id.home_popular_text);
        nearbyText = (TextView) rootView.findViewById(R.id.home_nearby_text);
        newStoresText = (TextView) rootView.findViewById(R.id.home_new_text);
        lookingText = (TextView) rootView.findViewById(R.id.home_looking_text);
        viewall =(TextView)rootView.findViewById(R.id.viewall);
        viewall1 =(TextView)rootView.findViewById(R.id.viewall1);
        defaultIndicator = (CircleIndicator) rootView.findViewById(R.id.indicator);

        offertext1 = (TextView) rootView.findViewById(R.id.offer_text1);
        offertext2 = (TextView) rootView.findViewById(R.id.offer_text2);
        shareText1 = (TextView) rootView.findViewById(R.id.share_text1);
        shareText2 = (TextView) rootView.findViewById(R.id.share_text2);
        mapIcon = (ImageView) rootView.findViewById(R.id.home_map_icon);

        listLayout = (NestedScrollView) rootView.findViewById(R.id.list_layout);
        mapLayout = (LinearLayout) rootView.findViewById(R.id.map_layout);
        locationLayout = (LinearLayout) rootView.findViewById(R.id.location_layout);
        typesLayout = (RelativeLayout) rootView.findViewById(R.id.looking_layout);

        promotedViewPager = (ViewPager) rootView.findViewById(R.id.home_promoted_list);
        popuplarViewPager = (ViewPager) rootView.findViewById(R.id.home_popular_list);
        nearbyViewPager = (ViewPager) rootView.findViewById(R.id.home_nearby_list);
        newStoresViewPager = (ViewPager) rootView.findViewById(R.id.new_stores_list);
        bannersViewpage = (ViewPager) rootView.findViewById(R.id.home_banner_list);
        lookingStoresViewPager = (ViewPager) rootView.findViewById(R.id.home_looking_list);

        shimmerPromotedLayout = (ShimmerLayout) rootView.findViewById(R.id.shimmer_promoted_layout);
        shimmerPopularLayout = (ShimmerLayout) rootView.findViewById(R.id.shimmer_popular_layout);
        shimmerNearbyLayout = (ShimmerLayout) rootView.findViewById(R.id.shimmer_nearby_layout);
        shimmerNewStoresLayout = (ShimmerLayout) rootView.findViewById(R.id.shimmer_new_stores_layout);
        storesListView = (ShimmerRecyclerView) rootView.findViewById(R.id.home_stores_list);

        shimmerNearbyLayout.startShimmerAnimation();
        shimmerNewStoresLayout.startShimmerAnimation();
        shimmerPopularLayout.startShimmerAnimation();
        shimmerPromotedLayout.startShimmerAnimation();
        storesListView.showShimmerAdapter();

        promotedlayout = (LinearLayout) rootView.findViewById(R.id.promoted_layout);
        populerlayout = (LinearLayout) rootView.findViewById(R.id.popular_layout);
        newlayout = (LinearLayout) rootView.findViewById(R.id.new_stores_layout);
        aroundlayout = (LinearLayout) rootView.findViewById(R.id.nearby_layout);
        bannerlayout =(LinearLayout) rootView.findViewById(R.id.banner_layout);
        lookigsomthinglayout =(RelativeLayout) rootView.findViewById(R.id.looking_layout);

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }
            else {
                try {
                    gps = new GPSTracker(getActivity());
                    getGPSCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            try {
                gps = new GPSTracker(getActivity());
                getGPSCoordinates();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        viewall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ViewallScreenActivity.class);
                startActivity(intent);
            }
        });
        viewall1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                Fragment SearchFragment = new SearchFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, SearchFragment).commit();
            }
        });

        if(storesList.size()>0){
            mStoreAdapter = new StoresAdapter(getContext(), brandslist, getActivity());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            storesListView.setLayoutManager(mLayoutManager);
            storesListView.setAdapter(mStoreAdapter);
            mPromotedAdapter = new PromotedStoresAdapter(getContext(), storesList);
            mPopularAdapter = new PopularStoresAdapter(getContext(), storesList);
            mAround = new Around3kmStoresAdapter(getContext(), storesList);
            mNewAdapter = new NewStoresAdapter(getContext(), storesList);
            mBanners = new BannersAdapter(getContext(),bannerList);
            mLookingAdapter = new HomeLookingForStoresAdapter(getContext(), somthingList);
            promotedViewPager.setAdapter(mPromotedAdapter);
            popuplarViewPager.setAdapter(mPopularAdapter);
            nearbyViewPager.setAdapter(mAround);
            newStoresViewPager.setAdapter(mNewAdapter);
            bannersViewpage.setAdapter(mBanners);
            lookingStoresViewPager.setAdapter(mLookingAdapter);

            shimmerPromotedLayout.stopShimmerAnimation();
            shimmerPopularLayout.stopShimmerAnimation();
            shimmerNearbyLayout.stopShimmerAnimation();
            shimmerNewStoresLayout.stopShimmerAnimation();
//            shimmerSomthingLayout.stopShimmerAnimation();

            shimmerPromotedLayout.setVisibility(View.GONE);
            shimmerPopularLayout.setVisibility(View.GONE);
            shimmerNearbyLayout.setVisibility(View.GONE);
            shimmerNewStoresLayout.setVisibility(View.GONE);

            promotedViewPager.setVisibility(View.VISIBLE);
            popuplarViewPager.setVisibility(View.VISIBLE);
            nearbyViewPager.setVisibility(View.VISIBLE);
            newStoresViewPager.setVisibility(View.VISIBLE);
            bannersViewpage.setVisibility(View.VISIBLE);
//            typesLayout.setVisibility(View.VISIBLE);
            lookingStoresViewPager.setVisibility(View.VISIBLE);
        }
//        if (brandslist.size() == 0) {
//            Constants.showOneButtonAlertDialog("Stores Not Availabe", getResources().getString(R.string.appname),
//                    getResources().getString(R.string.Done), getActivity());
//        }

        lookigsomthinglayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
//                    Log.d("TAG", "storeArrayList "+storesArrayList.size());
//                    Log.d("TAG", "pos1"+getAdapterPosition());
                args.putSerializable("array2", storesList);
//                args.putInt("pos2", getArguments());

                final SearchListDialog newFragment = SearchListDialog.newInstance();
                newFragment.setCancelable(true);
                newFragment.setArguments(args);
                newFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), "store1");
//                    Pair<View, String> p1 = Pair.create((View)store_image, "store_image");
//                    Pair<View, String> p2 = Pair.create((View)store_name, "store_name");
//                    Pair<View, String> p3= Pair.create((View)store_type, "store_type");
//                    Intent intent = new Intent(context, StoreMenuActivity.class);
//                    intent.putExtra("array", storesArrayList.get(getPosition()));
//                    ActivityOptionsCompat options = ActivityOptionsCompat.
//                            makeSceneTransitionAnimation(activity, p1, p2, p3);
//                    context.startActivity(intent, options.toBundle());
            }
        });


        new GetStoresApi().execute();
        locationLayout.setOnClickListener(this);
        mapIcon.setOnClickListener(this);

        LocalBroadcastManager.getInstance(context).registerReceiver(
                mMessageReceiver, new IntentFilter("GPSLocationUpdates"));
//        ProgressDialog progress = new ProgressDialog(getContext());
//        progress.setTitle("Loading");
//        progress.setMessage("Wait while loading...");
//        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
//        progress.show();
//// To dismiss the dialog
//        progress.dismiss();
        return rootView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void flipCard() {
        FlipAnimation flipAnimation = new FlipAnimation(listLayout, mapLayout);

        if (listLayout.getVisibility() == View.GONE) {
            flipAnimation.reverse();
            mapIcon.setImageDrawable(getResources().getDrawable(R.drawable.home_screen_map_icon));
        }
        else {
            mapIcon.setImageDrawable(getResources().getDrawable(R.drawable.tablelist));
        }
        mapLayout.startAnimation(flipAnimation);
    }

    private void setTypeface(){
        localityTitle.setTypeface(Constants.getTypeFace(context));
        popularText.setTypeface(Constants.getTypeFace(context));
        nearbyText.setTypeface(Constants.getTypeFace(context));
        newStoresText.setTypeface(Constants.getTypeFace(context));
        lookingText.setTypeface(Constants.getTypeFace(context));
        offertext1.setTypeface(Constants.getTypeFace(context));
        offertext2.setTypeface(Constants.getTypeFace(context));
        shareText1.setTypeface(Constants.getTypeFace(context));
        shareText2.setTypeface(Constants.getTypeFace(context));
    }

    private boolean canAccessLocation() {
        return (hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(context, perm));
    }

    public void getGPSCoordinates() throws IOException {

        if(gps != null){
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
                Log.i("TAG","fused lat "+latitude);
                Log.i("TAG","fused long "+longitude);

                try {

                    mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("TAG","fused lat null");
                        }
                    })
                            .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();
                                        Log.i("TAG","fused lat "+location.getLatitude());
                                        Log.i("TAG","fused long "+location.getLongitude());
                                    }
                                    else{
                                        Log.i("TAG","fused lat null");
                                    }
                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(getActivity());
                    try {
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(getActivity(), "Location permission denied, Unable to show nearby resorts", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(gps != null) {
            gps.stopUsingGPS();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(gps != null) {
            gps.stopUsingGPS();
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onBroadcastReceive: ");
            // Get extra data included in the Intent
            Bundle b = intent.getBundleExtra("Location");
            Location location = (Location) b.getParcelable("Location");
            if (location != null) {
                if(isCurrentLocationSelected) {
                    currentLatitude = location.getLatitude();
                    currentLongitude = location.getLongitude();
                    if(mMap != null) {
                        mMap.clear();
                        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(latLng);
                        mMap.addMarker(markerOptions);
                    }
                }
                getLocality();
            }
        }
    };

    public void getLocality(){
        Location mLocation = new Location("");
        mLocation.setLatitude(currentLatitude);
        mLocation.setLongitude(currentLongitude);

        Geocoder geocoder = null;
        try {
            geocoder = new Geocoder(context, Locale.getDefault());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Address found using the Geocoder.
        List<Address> addresses = null;
        String errorMessage = "";

        try {
            addresses = geocoder.getFromLocation(
                    mLocation.getLatitude(),
                    mLocation.getLongitude(),
                    // In this sample, we get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = context.getString(R.string.service_not_available);
            Log.e(TAG, errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = context.getString(R.string.invalid_lat_long_used);
            Log.e(TAG, errorMessage + ". " +
                    "Latitude = " + mLocation.getLatitude() +
                    ", Longitude = " + mLocation.getLongitude(), illegalArgumentException);
        }

        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Log.e(TAG, errorMessage);
            }
        }
        else {
            Address address = addresses.get(0);
            Log.d(TAG, "getLocality: "+address);
            localityTitle.setText(address.getSubLocality());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.location_layout:
                Intent intent = new Intent(getContext(), ChangeAddressActivity.class);
                startActivityForResult(intent, ADDRESS_REQUEST);
                break;

            case R.id.home_map_icon:
                flipCard();
                break;
        }
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if(requestCode == ADDRESS_REQUEST && resultCode == RESULT_OK){
//          currentLatitude = data.getDoubleExtra("lat", 0);
//          currentLongitude = data.getDoubleExtra("longi", 0);
//          isCurrentLocationSelected = false;
//          if (mMap!=null) {
//              mMap.clear();
//              LatLng latLng = new LatLng(currentLatitude, currentLongitude);
//              mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//              mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
//              MarkerOptions markerOptions = new MarkerOptions();
//              markerOptions.position(latLng);
//              mMap.addMarker(markerOptions);
//          }
//          getLocality();
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.clear();
//        LatLng latLng = new LatLng(latitude, longitude);
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        mMap.addMarker(markerOptions);
    }

    private class GetStoresApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            dialog = new ACProgressFlower.Builder(getContext())
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<StoresList> call = apiService.getStoresList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<StoresList>() {
                @Override
                public void onResponse(Call<StoresList> call, Response<StoresList> response) {
                    Log.i("TAG", "store servies responce " + response);
                    if (response.isSuccessful()) {
                        StoresList stores = response.body();
                        if (stores.getStatus()) {
                            storesList = stores.getData().getStoresdetails();
                            bannerList = stores.getData().getBannerresult();
                            somthingList = stores.getData().getStorecategories();

                            for (int i = 0; i < storesList.size(); i++) {
                                if (!seletedbrands.contains(storesList.get(i).getBrandid())) {
                                    seletedbrands.add(storesList.get(i).getBrandid());
                                    Brands brands = new Brands();
                                    brands.setBrandId(storesList.get(i).getBrandid());
                                    brands.setBradNameEn(storesList.get(i).getBrandnameEn());
                                    brands.setBranddescAr(storesList.get(i).getBrandnameAr());
                                    brands.setBrandimageEn(storesList.get(i).getStoreimageEn());
                                    brands.setBranddescAr(storesList.get(i).getBranchdescriptionEn());
                                    brands.setBranddescEn(storesList.get(i).getBranchdescriptionEn());

                                    ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < storesList.size(); j++) {
                                        if (storesList.get(i).getBrandid() == storesList.get(j).getBrandid()) {
                                            fillterstore.add(storesList.get(j));
                                        }
                                    }
                                    brands.setBrands(fillterstore);
                                    brandslist.add(brands);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            Log.d(TAG, "onResponse" + bannerList);
                            Log.d(TAG, "onResponse " + stores.getData().getBannerresult());

                            mStoreAdapter = new StoresAdapter(getContext(), brandslist, getActivity());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            storesListView.setLayoutManager(mLayoutManager);
                            storesListView.setAdapter(mStoreAdapter);
                            bannersViewpage.setAdapter(mBanners);
//                            lookingStoresViewPager.setAdapter(mLookingAdapter);

                            for (int i = 0; i < storesList.size(); i++) {
                                if (storesList.get(i).isPopular()) {
                                    popularList.add(storesList.get(i));
                                }
                                if (storesList.get(i).isIsnew()) {
                                    itsnewList.add(storesList.get(i));
                                }
                                if (storesList.get(i).isIspromoted()) {
                                    promotedList.add(storesList.get(i));
                                }
                                if (storesList.get(i).getDistance() <= 3) {
                                    threekmList.add(storesList.get(i));
                                }
                                if (promotedList.size() == 0) {
                                    promotedlayout.setVisibility(View.GONE);
                                } else {
                                    promotedlayout.setVisibility(View.VISIBLE);
                                    mPromotedAdapter = new PromotedStoresAdapter(getContext(), promotedList);
                                    promotedViewPager.setAdapter(mPromotedAdapter);
                                }

                                if (popularList.size() == 0) {
                                    populerlayout.setVisibility(View.GONE);
                                } else {
                                    populerlayout.setVisibility(View.VISIBLE);
                                    mPopularAdapter = new PopularStoresAdapter(getContext(), popularList);
                                    popuplarViewPager.setAdapter(mPopularAdapter);
                                }

                                if (threekmList.size() == 0) {
                                    aroundlayout.setVisibility(View.GONE);
                                } else {
                                    aroundlayout.setVisibility(View.VISIBLE);
                                    mAround = new Around3kmStoresAdapter(getContext(), threekmList);
                                    nearbyViewPager.setAdapter(mAround);
                                }

                                if (itsnewList.size() == 0) {
                                    newlayout.setVisibility(View.GONE);
                                } else {
                                    newlayout.setVisibility(View.VISIBLE);
                                    mNewAdapter = new NewStoresAdapter(getContext(), itsnewList);
                                    newStoresViewPager.setAdapter(mNewAdapter);
                                }

                                if (bannerList.size() == 0) {
                                    bannerlayout.setVisibility(View.GONE);
                                } else {
                                    bannerlayout.setVisibility(View.VISIBLE);
                                    mBanners = new BannersAdapter(getContext(), bannerList);
                                    defaultIndicator.setViewPager(bannersViewpage);
                                    bannersViewpage.setAdapter(mBanners);
                                }
                                if (somthingList.size() == 0) {
                                    lookigsomthinglayout.setVisibility(View.GONE);
                                } else {
                                    lookigsomthinglayout.setVisibility(View.VISIBLE);
                                    mLookingAdapter = new HomeLookingForStoresAdapter(getContext(), somthingList);
                                    lookingStoresViewPager.setAdapter(mLookingAdapter);
                                }

                                shimmerPromotedLayout.stopShimmerAnimation();
                                shimmerPopularLayout.stopShimmerAnimation();
                                shimmerNearbyLayout.stopShimmerAnimation();
                                shimmerNewStoresLayout.stopShimmerAnimation();

                                shimmerPromotedLayout.setVisibility(View.GONE);
                                shimmerPopularLayout.setVisibility(View.GONE);
                                shimmerNearbyLayout.setVisibility(View.GONE);
                                shimmerNewStoresLayout.setVisibility(View.GONE);

                                promotedViewPager.setVisibility(View.VISIBLE);
                                popuplarViewPager.setVisibility(View.VISIBLE);
                                nearbyViewPager.setVisibility(View.VISIBLE);
                                newStoresViewPager.setVisibility(View.VISIBLE);
                                bannersViewpage.setVisibility(View.VISIBLE);
                                typesLayout.setVisibility(View.VISIBLE);
                                lookingStoresViewPager.setVisibility(View.VISIBLE);
                            }
                        } else {
                            String failureResponse = stores.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), getActivity());
                        }
                    } else {
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<StoresList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return "";
        }



    }
    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Latitude", latitude);
            parentObj.put("Longitude", longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }


}
