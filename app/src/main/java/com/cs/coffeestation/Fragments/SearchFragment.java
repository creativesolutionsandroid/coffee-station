package com.cs.coffeestation.Fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.coffeestation.Adapters.SearchScreenaAdapter;
import com.cs.coffeestation.Adapters.SearchmenuListAdapter;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.StoresList;
import com.cs.coffeestation.R;
import com.cs.coffeestation.Rest.APIInterface;
import com.cs.coffeestation.Rest.ApiClient;
import com.cs.coffeestation.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.coffeestation.Activities.ForgotPasswordActivity.TAG;

public class SearchFragment extends Fragment {

    View rootView;
    EditText inputsearch;
    public static String searchTex;
    int catSearchId;
    private RecyclerView brandsListView,storeslistview;
    public SearchScreenaAdapter mSearchdcreen;
    public SearchmenuListAdapter mSorelist;
    ArrayList<StoresList.StoresDetails> brandslist = new ArrayList<>();
    ArrayList<StoresList.StoresDetails> storesList = new ArrayList<>();
    String inputStr;
    TextView clearbtn;
    int pos = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.search_fragment, container, false);
        brandsListView = (RecyclerView) rootView.findViewById(R.id.list_item);
        clearbtn = (TextView) rootView.findViewById(R.id.clear);
        storeslistview = (RecyclerView) rootView.findViewById(R.id.list_showitem);
        inputsearch = (EditText) rootView.findViewById(R.id.search);

        inputsearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        storesList = HomeScreenFragment.storesList;
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
                mMessageReceiver, new IntentFilter("Searchtream"));
        Log.d(TAG, "bannerList" + storesList.size());
        if (storesList.size() > 0) {
            mSearchdcreen = new SearchScreenaAdapter(getContext(), HomeScreenFragment.somthingList, getActivity());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            brandsListView.setLayoutManager(new GridLayoutManager(getContext(), 2));
//            storesListView.setLayoutManager(mLayoutManager);
            brandsListView.setAdapter(mSearchdcreen);
        }


        inputsearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    searchTex = inputsearch.getText().toString();
                    String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (searchTex.length() > 3) {
                            inputStr = prepareVerifyMobileJson("", searchTex);
                            new SearchApi().execute();
                        }
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });

        inputsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        clearbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: ");
                inputsearch.setText("");
                brandsListView.setVisibility(View.VISIBLE);
                storeslistview.setVisibility(View.GONE);
            }
        });
        return rootView;
    }
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onBroadcastReceive: ");
            // Get extra data included in the Intent
            String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                inputStr = prepareVerifyMobileJson(""+intent.getIntExtra("id",0), "");
                inputsearch.setText(intent.getStringExtra("typename"));
                new SearchApi().execute();
                Log.d(TAG, "onReceive"+inputStr);
            } else {
                Toast.makeText(getActivity().getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            }
        }
    };


    private class SearchApi extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ACProgressFlower.Builder(getActivity())
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<StoresList> call = apiService.getStoresList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<StoresList>() {
                @Override
                public void onResponse(Call<StoresList> call, Response<StoresList> response) {
                    Log.i("TAG", "store servies responce " + response);
                    if (response.isSuccessful()) {
                        StoresList stores = response.body();

                        if (stores.getStatus()) {
                            brandslist = stores.getData().getStoresdetails();
                        }
                        Log.d(TAG, "onResponse " + storesList.size());

                    }
                    if (storesList.size()>0){
                        Log.d(TAG, "onResponse " + brandslist.size());
                        brandsListView.setVisibility(View.GONE);
                        storeslistview.setVisibility(View.VISIBLE);
                        mSorelist = new SearchmenuListAdapter(getContext(),brandslist, getActivity());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                        storeslistview.setLayoutManager(mLayoutManager);
                        storeslistview.setAdapter(mSorelist);
                    }
                    else {
                        brandsListView.setVisibility(View.VISIBLE);
                        storeslistview.setVisibility(View.GONE);
                    }
//                    if (brandslist.size() == 0) {
//                            Constants.showOneButtonAlertDialog("Stores Not Availabe", getResources().getString(R.string.appname),
//                                    getResources().getString(R.string.Done), getActivity());
//                        }
                    if (brandslist.size() == 0) {
                        Constants.showOneButtonAlertDialog("Stores Not Availabe", getResources().getString(R.string.appname),
                                getResources().getString(R.string.Done), getActivity());
                        inputsearch.setText("");
                        brandsListView.setVisibility(View.VISIBLE);
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<StoresList> call, Throwable t) {

                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson (String searchid,String searchtext1 ) {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("StoreCatId", searchid);
            parentObj.put("SearchText", searchtext1);
            parentObj.put("Latitude", HomeScreenFragment.latitude);
            parentObj.put("Longitude",HomeScreenFragment.longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }
}
