package com.cs.coffeestation.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cs.coffeestation.Activities.ChangePasswordActivity;
import com.cs.coffeestation.Activities.EditProfileActivity;
import com.cs.coffeestation.Activities.MainActivity;
import com.cs.coffeestation.Activities.ManageCards;
import com.cs.coffeestation.Activities.MoreActivity;
import com.cs.coffeestation.Activities.MyAddressActivity;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.R;

import static android.app.Activity.RESULT_OK;

public class AccountFragment extends Fragment implements View.OnClickListener{

    TextView tvManageAddress, tvChangePassword,  tvPayments, tvOffers, tvLogout, tvMore,tvManagecards;
    TextView tvName, tvMobile, tvEmail, tvEditProfile;
    String strName, strMobile, strEmail;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    Toolbar toolbar;

    private static String TAG = Constants.TAG;
    private static int EDIT_REQUEST = 1;

    View rootView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_account, container, false);
        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId","0");
        userPrefsEditor = userPrefs.edit();
        tvName = (TextView) rootView.findViewById(R.id.account_name);
        tvEmail = (TextView) rootView.findViewById(R.id.account_email);
        tvMobile = (TextView) rootView.findViewById(R.id.account_mobile);
        tvEditProfile = (TextView) rootView.findViewById(R.id.account_edit);

        tvOffers = (TextView) rootView.findViewById(R.id.account_offers);
        tvManagecards = (TextView) rootView.findViewById(R.id.account_managecards);
        tvMore = (TextView) rootView.findViewById(R.id.account_more);
        tvLogout = (TextView) rootView.findViewById(R.id.account_logout);
        tvPayments = (TextView) rootView.findViewById(R.id.account_payments);
        tvManageAddress = (TextView) rootView.findViewById(R.id.account_manage_address);
        tvChangePassword = (TextView) rootView.findViewById(R.id.account_change_password);
        setTypeface();

        tvName.setText(userPrefs.getString("name","-"));
        tvEmail.setText(userPrefs.getString("email","-"));
        tvMobile.setText("+"+userPrefs.getString("mobile","-")+" - ");

        tvEditProfile.setOnClickListener(this);
        tvManageAddress.setOnClickListener(this);
        tvChangePassword.setOnClickListener(this);
        tvManagecards.setOnClickListener(this);
        tvPayments.setOnClickListener(this);
        tvOffers.setOnClickListener(this);
        tvMore.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
        return rootView;
    }

    private void setTypeface(){
        tvName.setTypeface(Constants.getTypeFace(getContext()));
        tvMobile.setTypeface(Constants.getTypeFace(getContext()));
        tvEmail.setTypeface(Constants.getTypeFace(getContext()));
        tvEditProfile.setTypeface(Constants.getTypeFace(getContext()));
        tvManageAddress.setTypeface(Constants.getTypeFace(getContext()));
        tvChangePassword.setTypeface(Constants.getTypeFace(getContext()));
        tvPayments.setTypeface(Constants.getTypeFace(getContext()));
        tvOffers.setTypeface(Constants.getTypeFace(getContext()));
        tvLogout.setTypeface(Constants.getTypeFace(getContext()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.account_edit:
                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                intent.putExtra("name", userPrefs.getString("name","-"));
                intent.putExtra("email", userPrefs.getString("email","-"));
                intent.putExtra("mobile", userPrefs.getString("mobile","-"));
                startActivityForResult(intent, EDIT_REQUEST);
                break;
            case R.id.account_change_password:
                startActivity(new Intent(getContext(), ChangePasswordActivity.class));
                break;

            case R.id.account_manage_address:
                startActivity(new Intent(getContext(), MyAddressActivity.class));
                break;
            case R.id.account_managecards:
                startActivity(new Intent(getContext(), ManageCards.class));
                break;
            case R.id.account_more:
                startActivity(new Intent(getContext(), MoreActivity.class));
                break;

            case R.id.account_logout:
                userPrefsEditor.clear();
                userPrefsEditor.commit();
//                FragmentManager fragmentManager = getChildFragmentManager();
//                Fragment mainFragment = new HomeScreenFragment();
//                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();
                startActivity(new Intent(getContext(), MainActivity.class));
                getActivity().finish();
//                fini
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == EDIT_REQUEST && resultCode == RESULT_OK){
//            new DisplayProfileApi().execute();
            tvName.setText(userPrefs.getString("name","-"));
            tvEmail.setText(userPrefs.getString("email","-"));
            tvMobile.setText("+"+userPrefs.getString("mobile","-")+" - ");
        }
    }
}
