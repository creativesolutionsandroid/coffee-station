package com.cs.coffeestation.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.DisplayProfile;
import com.cs.coffeestation.R;
import com.cs.coffeestation.Rest.APIInterface;
import com.cs.coffeestation.Rest.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAccountActivity extends AppCompatActivity implements View.OnClickListener{

    TextView tvManageAddress, tvChangePassword, tvFavorites, tvPayments, tvOffers, tvLogout;
    TextView tvName, tvMobile, tvEmail, tvEditProfile;
    String strName, strMobile, strEmail;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    Context context;
    Toolbar toolbar;

    private static String TAG = "TAG";
    private static int EDIT_REQUEST = 1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        context = this;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId","0");
        userPrefsEditor = userPrefs.edit();

        tvName = (TextView) findViewById(R.id.account_name);
        tvEmail = (TextView) findViewById(R.id.account_email);
        tvMobile = (TextView) findViewById(R.id.account_mobile);
        tvEditProfile = (TextView) findViewById(R.id.account_edit);

        tvOffers = (TextView) findViewById(R.id.account_offers);
        tvLogout = (TextView) findViewById(R.id.account_logout);
        tvPayments = (TextView) findViewById(R.id.account_payments);
        tvFavorites = (TextView) findViewById(R.id.account_favorites);
        tvManageAddress = (TextView) findViewById(R.id.account_manage_address);
        tvChangePassword = (TextView) findViewById(R.id.account_change_password);

        setTypeface();

        tvName.setText(userPrefs.getString("name","-"));
        tvEmail.setText(userPrefs.getString("email","-"));
        tvMobile.setText("+"+userPrefs.getString("mobile","-")+" - ");

        tvEditProfile.setOnClickListener(this);
        tvManageAddress.setOnClickListener(this);
        tvChangePassword.setOnClickListener(this);
        tvFavorites.setOnClickListener(this);
        tvPayments.setOnClickListener(this);
        tvOffers.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTypeface(){
        tvName.setTypeface(Constants.getTypeFace(context));
        tvMobile.setTypeface(Constants.getTypeFace(context));
        tvEmail.setTypeface(Constants.getTypeFace(context));
        tvEditProfile.setTypeface(Constants.getTypeFace(context));
        tvManageAddress.setTypeface(Constants.getTypeFace(context));
        tvChangePassword.setTypeface(Constants.getTypeFace(context));
        tvFavorites.setTypeface(Constants.getTypeFace(context));
        tvPayments.setTypeface(Constants.getTypeFace(context));
        tvOffers.setTypeface(Constants.getTypeFace(context));
        tvLogout.setTypeface(Constants.getTypeFace(context));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.account_edit:
                Intent intent = new Intent(MyAccountActivity.this, EditProfileActivity.class);
                intent.putExtra("name", userPrefs.getString("name","-"));
                intent.putExtra("email", userPrefs.getString("email","-"));
                intent.putExtra("mobile", userPrefs.getString("mobile","-"));
                startActivityForResult(intent, EDIT_REQUEST);
                break;

            case R.id.account_change_password:
                startActivity(new Intent(MyAccountActivity.this, ChangePasswordActivity.class));
                break;

            case R.id.account_manage_address:
                startActivity(new Intent(MyAccountActivity.this, MyAddressActivity.class));
                break;

            case R.id.account_logout:
                userPrefsEditor.clear();
                userPrefsEditor.commit();
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == EDIT_REQUEST && resultCode == RESULT_OK){
//            new DisplayProfileApi().execute();
            tvName.setText(userPrefs.getString("name","-"));
            tvEmail.setText(userPrefs.getString("email","-"));
            tvMobile.setText("+"+userPrefs.getString("mobile","-")+" - ");
        }
    }

    private class DisplayProfileApi extends AsyncTask<String, Integer, String> {

        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareDisplayProfileJSON();
            dialog = new ACProgressFlower.Builder(MyAccountActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<DisplayProfile> call = apiService.displayProfile(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<DisplayProfile>() {
                @Override
                public void onResponse(Call<DisplayProfile> call, Response<DisplayProfile> response) {
                    if(response.isSuccessful()){
                        DisplayProfile displayProfile = response.body();
                        try {
                            strName = displayProfile.getSuccess().getFullname();
                            strMobile = displayProfile.getSuccess().getMobile();
                            strEmail = displayProfile.getSuccess().getEmail();
                            tvName.setText(strName);
                            tvEmail.setText(strEmail);
                            tvMobile.setText("+"+strMobile+" - ");
                        } catch (Exception e) {
                            e.printStackTrace();
                            String failureResponse = displayProfile.getFailure();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), MyAccountActivity.this);
                        }
                    }
                    else{
                        Toast.makeText(MyAccountActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<DisplayProfile> call, Throwable t) {
                    Toast.makeText(MyAccountActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(dialog != null){
                dialog.dismiss();
            }
        }
    }

    private String prepareDisplayProfileJSON(){
        JSONObject parentObj = new JSONObject();
        JSONObject profileObj = new JSONObject();

        try {
            profileObj.put("UserId",userId);
            parentObj.put("DisplayProfile",profileObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "prepareDisplayProfileJSON: "+parentObj.toString());
        return parentObj.toString();
    }
}
