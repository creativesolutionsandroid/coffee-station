package com.cs.coffeestation.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.UserRegistrationResponse;
import com.cs.coffeestation.R;
import com.cs.coffeestation.Rest.APIInterface;
import com.cs.coffeestation.Rest.ApiClient;
import com.cs.coffeestation.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener{

    TextInputLayout inputLayoutName, inputLayoutEmail;
    EditText inputName, inputMobile, inputEmail;
    String strName, strEmail, strMobile, strPassword;
    Button buttonSubmit;
    Toolbar toolbar;
    Context context;
    String language, userId;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    SharedPreferences.Editor userPrefsEditor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        userId = userPrefs.getString("userId","0");
        userPrefsEditor = userPrefs.edit();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        buttonSubmit = (Button) findViewById(R.id.button_submit);
        inputName = (EditText) findViewById(R.id.edit_profile_input_name);
        inputEmail = (EditText) findViewById(R.id.edit_profile_input_email);
        inputMobile = (EditText) findViewById(R.id.edit_profile_input_mobile);

        inputLayoutName = (TextInputLayout) findViewById(R.id.input_layout_name);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);

        inputName.setText(getIntent().getStringExtra("name"));
        inputEmail.setText(getIntent().getStringExtra("email"));
        inputMobile.setText("+"+getIntent().getStringExtra("mobile"));

        setTypeface();
        setFilters();

        buttonSubmit.setOnClickListener(this);

        inputName.addTextChangedListener(new TextWatcher(inputName));
        inputEmail.addTextChangedListener(new TextWatcher(inputEmail));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTypeface(){
        inputName.setTypeface(Constants.getTypeFace(context));
        inputEmail.setTypeface(Constants.getTypeFace(context));
        buttonSubmit.setTypeface(Constants.getTypeFace(context));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_submit:
                if(validations()){
                    String networkStatus = NetworkUtil.getConnectivityStatusString(EditProfileActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new editProfileApi().execute();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private boolean validations(){
        strName = inputName.getText().toString().trim();
        strEmail = inputEmail.getText().toString().trim();
        strMobile = getIntent().getStringExtra("mobile");

        if (strName.length() <= 3){
            inputLayoutName.setError(getResources().getString(R.string.signup_msg_invalid_name));
            Constants.requestEditTextFocus(inputName, EditProfileActivity.this);
            return false;
        }
        else if (strEmail.length() == 0){
            inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            Constants.requestEditTextFocus(inputEmail, EditProfileActivity.this);
            return false;
        }
        else if (!Constants.isValidEmail(strEmail)){
            inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_invalid_email));
            Constants.requestEditTextFocus(inputEmail, EditProfileActivity.this);
            return false;
        }
        return true;
    }

    private void clearErrors(){
        inputLayoutName.setErrorEnabled(false);
        inputLayoutEmail.setErrorEnabled(false);
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edit_profile_input_name:
                    if(editable.toString().startsWith(" ")){
                        inputName.setText("");
                    }
                    clearErrors();
                    break;
                case R.id.edit_profile_input_email:
                    clearErrors();
                    break;
            }
        }
    }

    private String prepareEditProfileJson(){
        JSONObject parentObj = new JSONObject();
        JSONObject userDetailsObj = new JSONObject();
        JSONObject userAuthObj = new JSONObject();

        try {
            userDetailsObj.put("UserId", userId);
            userDetailsObj.put("FullName", strName);
            userDetailsObj.put("FamilyName","");
            userDetailsObj.put("NickName","");
            userDetailsObj.put("Gender","");
            userDetailsObj.put("Mobile", strMobile);
            userDetailsObj.put("Email", strEmail);
            userDetailsObj.put("Password","");
            userDetailsObj.put("Language", language);
            userDetailsObj.put("DeviceType", Constants.getDeviceType(context));
            userDetailsObj.put("UserType", Constants.UserType);
            userAuthObj.put("DeviceToken","-1");

            parentObj.put("UserDetails", userDetailsObj);
            parentObj.put("UserAuthActivity",userAuthObj);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(Constants.TAG, "prepareEditProfileJson: "+parentObj.toString());
        return parentObj.toString();
    }

    private class editProfileApi extends AsyncTask<String, Integer, String>{

        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareEditProfileJson();
            dialog = new ACProgressFlower.Builder(EditProfileActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(EditProfileActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<UserRegistrationResponse> call = apiService.userRegistration(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UserRegistrationResponse>() {
                @Override
                public void onResponse(Call<UserRegistrationResponse> call, Response<UserRegistrationResponse> response) {
                    if(response.isSuccessful()){
                        UserRegistrationResponse registrationResponse = response.body();
                        if(registrationResponse.getStatus()){
//                          status true case
                            String userId = registrationResponse.getData().getUserid();
                            userPrefsEditor.putString("userId", userId);
                            userPrefsEditor.putString("name", registrationResponse.getData().getName());
                            userPrefsEditor.putString("email", registrationResponse.getData().getEmail());
                            userPrefsEditor.putString("mobile", registrationResponse.getData().getMobile());
                            userPrefsEditor.commit();
                            finish();
                        }
                        else {
//                          status false case
                            String failureResponse = registrationResponse.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), EditProfileActivity.this);
                        }
                    }
                    else{
                        if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            Toast.makeText(EditProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(dialog != null){
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<UserRegistrationResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(EditProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if(dialog != null){
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    public void setFilters() {
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                boolean keepOriginal = true;
                StringBuilder sb = new StringBuilder(end - start);
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (isCharAllowed(c)) // put your condition here
                        sb.append(c);
                    else
                        keepOriginal = false;
                }
                if (keepOriginal)
                    return null;
                else {
                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(sb);
                        TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                        return sp;
                    } else {
                        return sb;
                    }
                }
            }

            private boolean isCharAllowed(char c) {
                return Character.isLetterOrDigit(c) || Character.isSpaceChar(c);
            }
        };

        inputName.setFilters(new InputFilter[] { filter });
    }
}
