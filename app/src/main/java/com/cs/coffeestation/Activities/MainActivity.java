package com.cs.coffeestation.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.cs.coffeestation.Adapters.SideMenuAdapter;
import com.cs.coffeestation.Fragments.AccountFragment;
import com.cs.coffeestation.Fragments.HomeScreenFragment;
import com.cs.coffeestation.Fragments.SearchFragment;
import com.cs.coffeestation.R;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity {


    FragmentManager fragmentManager = getSupportFragmentManager();
    private static int ACCOUNT_INTENT = 1;
    String language, userId;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Fragment mainFragment = new HomeScreenFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();
                    return true;
                case R.id.navigation_search:
                    Fragment searchFragment = new SearchFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, searchFragment).commit();
                    return true;

                case R.id.navigation_cart:

                    return true;
                case R.id.navigation_more:
                    userId = userPrefs.getString("userId","0");
                    if(userId.equals("0")){
                        startActivityForResult(new Intent(MainActivity.this, SignInActivity.class), ACCOUNT_INTENT);
                    }
                    else{
                        Fragment accountFragment = new AccountFragment();
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment).commit();
                    }
                    return true;
            }
            return false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        userId = userPrefs.getString("userId","0");

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        disableShiftMode(navigation);

        Fragment mainFragment = new HomeScreenFragment();
        fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ACCOUNT_INTENT && resultCode == RESULT_OK){
            Fragment accountFragment = new AccountFragment();
            FragmentTransaction ft = fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment);
            ft.commit();
        }
        else if (resultCode == RESULT_CANCELED){

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    // Method for disabling ShiftMode of BottomNavigationView
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                item.setPadding(0, 15, 0, 0);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

}
