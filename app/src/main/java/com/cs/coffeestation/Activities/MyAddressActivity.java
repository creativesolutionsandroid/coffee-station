package com.cs.coffeestation.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.cs.coffeestation.Adapters.MyAddressAdapter;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.MyAddress;
import com.cs.coffeestation.R;
import com.cs.coffeestation.Rest.APIInterface;
import com.cs.coffeestation.Rest.ApiClient;
import com.cs.coffeestation.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAddressActivity extends AppCompatActivity implements View.OnClickListener{

//    private TextView buttonHome, buttonWork, buttonOther;
    private Button buttonAddAddress;
    private ArrayList<MyAddress.Data> myAddressArrayList = new ArrayList<>();
    private MyAddressAdapter mAddressAdapter;
    private ShimmerRecyclerView addressList;
    private TextView addressAlert;
    ACProgressFlower dialog;
    ImageView backbtn;
    Toolbar toolbar;
    String userId;
    SharedPreferences userPrefs;
    private static String TAG = "TAG";
    public static int ADD_ADDRESS_INTENT = 1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_address);



        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId","0");

//        buttonHome = (TextView) findViewById(R.id.button_address_home);
//        buttonWork = (TextView) findViewById(R.id.button_address_work);
//        buttonOther = (TextView) findViewById(R.id.button_address_other);
        buttonAddAddress = (Button) findViewById(R.id.add_address);
        addressAlert = (TextView) findViewById(R.id.no_address_alert);
        addressList = (ShimmerRecyclerView) findViewById(R.id.address_list);
        backbtn =(ImageView)findViewById(R.id.backbtn) ;
        addressList.showShimmerAdapter();

        String networkStatus = NetworkUtil.getConnectivityStatusString(MyAddressActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new GetMyAddressApi().execute();
        }
        else{
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

        buttonAddAddress.setOnClickListener(this);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add_address:
                Intent intent = new Intent(MyAddressActivity.this, AddAddressActivity.class);
                startActivityForResult(intent, ADD_ADDRESS_INTENT);
                break;

//            case R.id.button_address_home:
//                buttonHome.setBackground(getResources().getDrawable(R.drawable.my_address_left_shape_selected));
//                buttonWork.setBackground(getResources().getDrawable(R.drawable.my_address_middle_shape_unselected));
//                buttonOther.setBackground(getResources().getDrawable(R.drawable.my_address_right_shape_unselected));
//                break;
//
//            case R.id.button_address_work:
//                buttonHome.setBackground(getResources().getDrawable(R.drawable.my_address_left_shape_unselected));
//                buttonWork.setBackground(getResources().getDrawable(R.drawable.my_address_middle_shape_selected));
//                buttonOther.setBackground(getResources().getDrawable(R.drawable.my_address_right_shape_unselected));
//                break;
//
//            case R.id.button_address_other:
//                buttonHome.setBackground(getResources().getDrawable(R.drawable.my_address_left_shape_unselected));
//                buttonWork.setBackground(getResources().getDrawable(R.drawable.my_address_middle_shape_unselected));
//                buttonOther.setBackground(getResources().getDrawable(R.drawable.my_address_right_shape_selected));
//                break;
        }
    }

    private class GetMyAddressApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareDisplayProfileJSON();
            dialog = new ACProgressFlower.Builder(MyAddressActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<MyAddress> call = apiService.myAddress(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<MyAddress>() {
                @Override
                public void onResponse(Call<MyAddress> call, Response<MyAddress> response) {
                    if(response.isSuccessful()){
                        MyAddress myAddress = response.body();
                        if(myAddress.getStatus()){
                            myAddressArrayList = myAddress.getData();
                            if(dialog != null){
                                dialog.hide();
                            }
                            if(myAddressArrayList.size() > 0) {
                                mAddressAdapter = new MyAddressAdapter(MyAddressActivity.this, myAddressArrayList, MyAddressActivity.this);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyAddressActivity.this);
                                addressList.setLayoutManager(mLayoutManager);
                                addressList.setAdapter(mAddressAdapter);
                                mAddressAdapter.notifyDataSetChanged();
                                addressAlert.setVisibility(View.GONE);
                            }
                            else {
                                addressAlert.setVisibility(View.VISIBLE);
                            }
                        }
                        else{
                            addressAlert.setVisibility(View.VISIBLE);
                            String failureResponse = myAddress.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), MyAddressActivity.this);
                        }
                    }
                    else{
                        addressAlert.setVisibility(View.VISIBLE);
                        Toast.makeText(MyAddressActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    if(dialog != null){
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<MyAddress> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(MyAddressActivity.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(MyAddressActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(MyAddressActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    addressAlert.setVisibility(View.VISIBLE);
                    if(dialog != null){
                        dialog.dismiss();
                    }
                }
            });
            return "";
        }
    }

    private String prepareDisplayProfileJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId",userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ADD_ADDRESS_INTENT && resultCode == RESULT_OK){
//            Collections.reverse(myAddressArrayList);
//            myAddressArrayList.addAll((ArrayList<MyAddress.Data>)data.getSerializableExtra("newAddress"));
//            Collections.reverse(myAddressArrayList);
//            mAddressAdapter.notifyDataSetChanged();
//            addressList.scrollTo(0,0);
            String networkStatus = NetworkUtil.getConnectivityStatusString(MyAddressActivity.this);
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                new GetMyAddressApi().execute();
            }
            else{
                Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        if(dialog != null){
            dialog.dismiss();
        }
        super.onDestroy();
    }
}
