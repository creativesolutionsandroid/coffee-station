package com.cs.coffeestation.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.coffeestation.Adapters.NewStoresAdapter;
import com.cs.coffeestation.Adapters.PViewallAdapter;
import com.cs.coffeestation.Fragments.HomeScreenFragment;
import com.cs.coffeestation.Models.StoresList;
import com.cs.coffeestation.R;

import java.util.ArrayList;

import static com.cs.coffeestation.Fragments.HomeScreenFragment.storesList;

public class ViewallScreenActivity extends AppCompatActivity {
    TextView header;
    ImageView backbtn;
    RecyclerView listview;
    public static final String TAG = "TAG";
    private ArrayList<StoresList.StoresDetails> storeItemsArrayList = new ArrayList<>();
    PViewallAdapter mViewalldAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewallactiviry);

        header =(TextView)findViewById(R.id.headertext);
        backbtn =(ImageView)findViewById(R.id.back_btn);
        listview =(RecyclerView)findViewById(R.id.list_item);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//        if(storeItemsArrayList.size()>0){
            mViewalldAdapter = new PViewallAdapter(ViewallScreenActivity.this, HomeScreenFragment.promotedList,ViewallScreenActivity.this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ViewallScreenActivity.this);
            listview.setLayoutManager(mLayoutManager);
            listview.setAdapter(mViewalldAdapter);
            Log.d(TAG, "viewall"+storeItemsArrayList.size());
//        }
    }
}

