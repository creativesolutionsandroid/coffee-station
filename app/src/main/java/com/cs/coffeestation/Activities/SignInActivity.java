package com.cs.coffeestation.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.UserRegistrationResponse;
import com.cs.coffeestation.R;
import com.cs.coffeestation.Rest.APIInterface;
import com.cs.coffeestation.Rest.ApiClient;
import com.cs.coffeestation.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    String language;
    Toolbar toolbar;
    Context context;
    Button buttonSignIn;
    TextView textRegister;
    ImageButton imagePasswordEye;
    SharedPreferences userPrefs;
    String strMobile, strPassword;
    SharedPreferences LanguagePrefs;
    EditText inputPassword;
    EditText inputMobile;
    SharedPreferences.Editor userPrefsEditor;
    LinearLayout registerLayout, forgotPasswordLayout;
    TextInputLayout inputLayoutMobile, inputLayoutPassword;

    private static int SIGNUP_REQUEST = 1;
    private static int FORGOT_PASSWORD_REQUEST = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        userPrefsEditor = userPrefs.edit();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        buttonSignIn = (Button) findViewById(R.id.signin_button);
        textRegister = (TextView) findViewById(R.id.signin_register);
        inputMobile = (com.bachors.prefixinput.EditText) findViewById(R.id.signin_input_mobile);
        inputPassword= (EditText) findViewById(R.id.signin_input_password);
        imagePasswordEye = (ImageButton) findViewById(R.id.signin_image_password);
        inputLayoutMobile = (TextInputLayout) findViewById(R.id.input_layout_mobile);
        inputLayoutPassword= (TextInputLayout) findViewById(R.id.layout_password);
        registerLayout = (LinearLayout) findViewById(R.id.signin_register_layout);
        forgotPasswordLayout = (LinearLayout) findViewById(R.id.signin_forgot_password);

        inputMobile.setText(Constants.Country_Code);

        setTypeface();

        inputMobile.addTextChangedListener(new TextWatcher(inputMobile));
        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTypeface(){
        inputMobile.setTypeface(Constants.getTypeFace(context));
        inputPassword.setTypeface(Constants.getTypeFace(context));
        buttonSignIn.setTypeface(Constants.getTypeFace(context));
        textRegister.setTypeface(Constants.getTypeFace(context));
    }

    public void userSignIn(View view){
        if(validations()){
            String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                userLoginApi();
            }
            else{
                Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void launchSignUpActivity(View view){
        Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
        startActivityForResult(intent, SIGNUP_REQUEST);
    }

    public void launchForgotPasswordActivity(View view){
        Intent intent1 = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
        startActivityForResult(intent1, FORGOT_PASSWORD_REQUEST);
    }

    public void tooglePasswordMode(View view){
        if(inputPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD){
            inputPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            imagePasswordEye.setImageDrawable(getResources().getDrawable(R.drawable.signup_password_visible));
        }
        else{
            inputPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
            imagePasswordEye.setImageDrawable(getResources().getDrawable(R.drawable.signup_password_invisible));
        }
        inputPassword.setSelection(inputPassword.length());
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SIGNUP_REQUEST && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
        else if(requestCode == FORGOT_PASSWORD_REQUEST && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }

    private boolean validations(){
        strMobile = inputMobile.getText().toString();
        strPassword = inputPassword.getText().toString();
        strMobile = strMobile.replace("+966 ","");

        if (strMobile.length() == 0){
            inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            Constants.requestEditTextFocus(inputMobile, SignInActivity.this);
            return false;
        }
        else if (strMobile.length() != 9){
            inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
            Constants.requestEditTextFocus(inputMobile, SignInActivity.this);
            return false;
        }
        else if (strPassword.length() == 0){
            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            Constants.requestEditTextFocus(inputPassword, SignInActivity.this);
            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 20){
            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            Constants.requestEditTextFocus(inputPassword, SignInActivity.this);
            return false;
        }
        return true;
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.signin_input_mobile:
                    String enteredMobile = editable.toString();
                    if(!enteredMobile.contains(Constants.Country_Code)){
                        if(enteredMobile.length() > Constants.Country_Code.length()){
                            enteredMobile = enteredMobile.substring((Constants.Country_Code.length() - 1));
                            inputMobile.setText(Constants.Country_Code + enteredMobile);
                        }
                        else {
                            inputMobile.setText(Constants.Country_Code);
                        }
                        inputMobile.setSelection(inputMobile.length());
                    }
                    clearErrors();
                    break;
                case R.id.signin_input_password:
                    clearErrors();
                    if(editable.length() > 20){
                        inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
            }
        }
    }

    private void clearErrors(){
        inputLayoutMobile.setErrorEnabled(false);
        inputLayoutPassword.setErrorEnabled(false);
    }

//    private class userLoginApi extends AsyncTask<String, Integer, String>{
//
//        ACProgressFlower dialog;
//        String inputStr;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            inputStr = prepareLoginJson();
//            dialog = new ACProgressFlower.Builder(SignInActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
//            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//
//            Call<UserRegistrationResponse> call = apiService.userLogin(
//                    RequestBody.create(MediaType.parse("application/json"), inputStr));
//            call.enqueue(new Callback<UserRegistrationResponse>() {
//                @Override
//                public void onResponse(Call<UserRegistrationResponse> call, Response<UserRegistrationResponse> response) {
//                    if(response.isSuccessful()){
//                        UserRegistrationResponse registrationResponse = response.body();
//                        try {
//                            if(registrationResponse.getStatus()){
//    //                          status true case
//                                String userId = registrationResponse.getData().getUserid();
//                                userPrefsEditor.putString("userId", userId);
//                                userPrefsEditor.putString("name", registrationResponse.getData().getName());
//                                userPrefsEditor.putString("email", registrationResponse.getData().getEmail());
//                                userPrefsEditor.putString("mobile", registrationResponse.getData().getMobile());
//                                userPrefsEditor.commit();
//                                Toast.makeText(SignInActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
//                                setResult(RESULT_OK);
//                                finish();
//                            }
//                            else {
//    //                          status false case
//                                String failureResponse = registrationResponse.getMessage();
//                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
//                                        getResources().getString(R.string.ok), SignInActivity.this);
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    else{
//                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<UserRegistrationResponse> call, Throwable t) {
//                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        Toast.makeText(SignInActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                    }
//                    else {
//                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if(dialog != null){
//                dialog.dismiss();
//            }
//        }
//    }

    private void userLoginApi(){
        String inputStr = prepareLoginJson();
        final ACProgressFlower dialog = new ACProgressFlower.Builder(SignInActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();;
        dialog.show();
        final String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<UserRegistrationResponse> call = apiService.userLogin(
                RequestBody.create(MediaType.parse("application/json"), inputStr));

        call.enqueue(new Callback<UserRegistrationResponse>() {
            @Override
            public void onResponse(Call<UserRegistrationResponse> call, Response<UserRegistrationResponse> response) {
                if(response.isSuccessful()){
                    UserRegistrationResponse registrationResponse = response.body();
                    try {
                        if(registrationResponse.getStatus()){
                            //status true case
                            String userId = registrationResponse.getData().getUserid();
                            userPrefsEditor.putString("userId", userId);
                            userPrefsEditor.putString("name", registrationResponse.getData().getName());
                            userPrefsEditor.putString("email", registrationResponse.getData().getEmail());
                            userPrefsEditor.putString("mobile", registrationResponse.getData().getMobile());
                            userPrefsEditor.commit();
                            Toast.makeText(SignInActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                            setResult(RESULT_OK);
                            finish();
                        }
                        else {
                            //status false case
                            String failureResponse = registrationResponse.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), SignInActivity.this);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<UserRegistrationResponse> call, Throwable t) {
                if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(SignInActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                dialog.dismiss();
            }
        });
    }

    private String prepareLoginJson(){
        JSONObject parentObj = new JSONObject();
        JSONObject userDetailsObj = new JSONObject();
        JSONObject userAuthObj = new JSONObject();

        try {
            userDetailsObj.put("Mobile","966"+strMobile);
            userDetailsObj.put("Password", strPassword);
            userAuthObj.put("DeviceToken","-1");

            parentObj.put("UserDetails", userDetailsObj);
            parentObj.put("UserAuthActivity",userAuthObj);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", " prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }
}
