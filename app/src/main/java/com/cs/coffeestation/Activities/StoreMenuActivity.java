package com.cs.coffeestation.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.cs.coffeestation.Adapters.MenuItemsAdapter;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.MenuItems;
import com.cs.coffeestation.Models.MyAddress;
import com.cs.coffeestation.Models.StoresList;
import com.cs.coffeestation.R;
import com.cs.coffeestation.Rest.APIInterface;
import com.cs.coffeestation.Rest.ApiClient;
import com.cs.coffeestation.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreMenuActivity extends AppCompatActivity {

    private ArrayList<MenuItems.Data> menuItemsArrayList = new ArrayList<>();
    private MenuItemsAdapter mMenuItemsAdapter;
    private ShimmerRecyclerView itemsList;
    private TextView storeName, storeType;
    private ArrayList<String> headers = new ArrayList<>();
    Toolbar toolbar;
    String userId;
    SharedPreferences userPrefs;
    private Context context;
    StoresList.Data storeData = new StoresList.Data();
    ImageView storeImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_menu);
        context = this;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        storeData = (StoresList.Data) getIntent().getSerializableExtra("array");

        final CollapsingToolbarLayout ctl = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    ctl.setTitle(storeData.getBannerresult().get(0).getBannernameEn());
                    isShow = true;
                } else if(isShow) {
                    ctl.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });

        storeImage = (ImageView) findViewById(R.id.backdrop);
        storeName = (TextView) findViewById(R.id.store_name);
        storeType = (TextView) findViewById(R.id.store_type);

//        Glide.with(context)
//                .load(Constants.IMAGE_URL+storeData.getStoresdetails().get(0).)
//                .into(storeImage);
//        storeName.setText(storeData.getBranchnameEn());
//        storeType.setText(storeData.getStoretype());

        AppBarLayout mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                } else if (isShow) {
                    isShow = false;
                }
            }
        });

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId","0");

        itemsList = (ShimmerRecyclerView) findViewById(R.id.address_list);
        itemsList.showShimmerAdapter();

        String networkStatus = NetworkUtil.getConnectivityStatusString(StoreMenuActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new GetMenuItems().execute();
        }
        else{
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class GetMenuItems extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareMenuItemsJson();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<MenuItems> call = apiService.getMenuItems(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<MenuItems>() {
                @Override
                public void onResponse(Call<MenuItems> call, Response<MenuItems> response) {
                    if(response.isSuccessful()) {
                        MenuItems menuItemsResponse = response.body();
                        try {
                            if(menuItemsResponse.getStatus()){
                                menuItemsArrayList = menuItemsResponse.getData();
                                ArrayList<MenuItems.Data> finalMenuList = new ArrayList<>();
                                ArrayList<String> displayedHeaders = new ArrayList<>();

                                for (MenuItems.Data data : menuItemsArrayList){
                                    if(!headers.contains(data.getMcatEn())){
                                        headers.add(data.getMcatEn());
                                    }
                                }

                                for (int i = 0; i < menuItemsArrayList.size(); i++) {
                                    if (!displayedHeaders.contains(menuItemsArrayList.get(i).getMcatEn())){
                                        displayedHeaders.add(menuItemsArrayList.get(i).getMcatEn());
                                        MenuItems.Data newDatawithHeader = new MenuItems.Data();
                                        newDatawithHeader = menuItemsArrayList.get(i);
                                        newDatawithHeader.setHeader(true);
                                        finalMenuList.add(newDatawithHeader);

                                        MenuItems.Data newDatawithoutHeader = new MenuItems.Data();
                                        newDatawithoutHeader = menuItemsArrayList.get(i);
                                        newDatawithoutHeader.setHeader(false);
                                        finalMenuList.add(newDatawithoutHeader);
                                    }
                                    else {
                                        MenuItems.Data newDatawithoutHeader = new MenuItems.Data();
                                        newDatawithoutHeader = menuItemsArrayList.get(i);
                                        newDatawithoutHeader.setHeader(false);
                                        finalMenuList.add(newDatawithoutHeader);
                                    }
                                }

                                mMenuItemsAdapter = new MenuItemsAdapter(StoreMenuActivity.this, finalMenuList, headers, StoreMenuActivity.this);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(StoreMenuActivity.this);
                                itemsList.setLayoutManager(mLayoutManager);
                                itemsList.setAdapter(mMenuItemsAdapter);
                            }
                            else {
                                String failureResponse = menuItemsResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), StoreMenuActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(StoreMenuActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<MenuItems> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(StoreMenuActivity.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(StoreMenuActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(StoreMenuActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return "";
        }
    }

    private String prepareMenuItemsJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("BrandId","3");
            parentObj.put("BranchId","1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }
}
