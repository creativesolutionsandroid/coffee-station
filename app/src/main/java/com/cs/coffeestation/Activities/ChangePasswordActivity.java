package com.cs.coffeestation.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.UserRegistrationResponse;
import com.cs.coffeestation.R;
import com.cs.coffeestation.Rest.APIInterface;
import com.cs.coffeestation.Rest.ApiClient;
import com.cs.coffeestation.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    TextInputLayout inputLayoutOldPassword, inputLayoutNewPassword, inputLayoutConfirmPassword;
    EditText inputOldPassword, inputNewPassword, inputConfirmPassword;
    String strOldPassword, strNewPassword, strConfirmPassword;
    Button buttonSubmit;
    Context context;
    ImageView backbtn;
    String language, userId;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        userId = userPrefs.getString("userId","0");



        buttonSubmit = (Button) findViewById(R.id.button_submit);
        backbtn = (ImageView) findViewById(R.id.backbtn);
        inputOldPassword = (EditText) findViewById(R.id.change_password_input_old_password);
        inputNewPassword = (EditText) findViewById(R.id.change_password_input_new_password);
        inputConfirmPassword = (EditText) findViewById(R.id.change_password_input_confirm_password);

        inputLayoutOldPassword = (TextInputLayout) findViewById(R.id.input_layout_old_password);
        inputLayoutNewPassword = (TextInputLayout) findViewById(R.id.input_layout_new_password);
        inputLayoutConfirmPassword = (TextInputLayout) findViewById(R.id.input_layout_confirm_password);

        setTypeface();

        inputOldPassword.addTextChangedListener(new TextWatcher(inputOldPassword));
        inputNewPassword.addTextChangedListener(new TextWatcher(inputNewPassword));
        inputConfirmPassword.addTextChangedListener(new TextWatcher(inputConfirmPassword));
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setTypeface(){
        inputOldPassword.setTypeface(Constants.getTypeFace(context));
        inputNewPassword.setTypeface(Constants.getTypeFace(context));
        inputConfirmPassword.setTypeface(Constants.getTypeFace(context));
        buttonSubmit.setTypeface(Constants.getTypeFace(context));
    }

    public void changePasswordClickEvents(View v) {
        switch (v.getId()){
            case R.id.button_submit:
                if(validations()){
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ChangePasswordActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new changePasswordApi().execute();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private boolean validations(){
        strOldPassword = inputOldPassword.getText().toString();
        strNewPassword = inputNewPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();

        if (strOldPassword.length() == 0){
            inputLayoutOldPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            Constants.requestEditTextFocus(inputOldPassword, ChangePasswordActivity.this);
            return false;
        }
        else if (strOldPassword.length() < 4 || strOldPassword.length() > 20){
            inputLayoutOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            Constants.requestEditTextFocus(inputOldPassword, ChangePasswordActivity.this);
            return false;
        }
        if (strNewPassword.length() == 0){
            inputLayoutNewPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            Constants.requestEditTextFocus(inputNewPassword, ChangePasswordActivity.this);
            return false;
        }
        else if (strNewPassword.length() < 4 || strNewPassword.length() > 20){
            inputLayoutOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            Constants.requestEditTextFocus(inputOldPassword, ChangePasswordActivity.this);
            return false;
        }
        else if (!strNewPassword.equals(strConfirmPassword)){
            inputLayoutConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match));
            Constants.requestEditTextFocus(inputConfirmPassword, ChangePasswordActivity.this);
            return false;
        }

        return true;
    }

    private void clearErrors(){
        inputLayoutOldPassword.setErrorEnabled(false);
        inputLayoutNewPassword.setErrorEnabled(false);
        inputLayoutConfirmPassword.setErrorEnabled(false);
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.change_password_input_old_password:
                    clearErrors();
                    if(editable.length() > 20){
                        inputLayoutOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
                case R.id.change_password_input_new_password:
                    clearErrors();
                    if(editable.length() > 20){
                        inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
                case R.id.change_password_input_confirm_password:
                    clearErrors();
                    if(editable.length() > 20){
                        inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
            }
        }
    }

    private String prepareChangePasswordJson(){
        JSONObject parentObj = new JSONObject();
        JSONObject changePasswordObj = new JSONObject();

        try {
            changePasswordObj.put("UserId", userId);
            changePasswordObj.put("OldPassword", strOldPassword);
            changePasswordObj.put("NewPassword", strNewPassword);

            parentObj.put("ChangePassword", changePasswordObj);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    private class changePasswordApi extends AsyncTask<String, Integer, String> {

        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareChangePasswordJson();
            dialog = new ACProgressFlower.Builder(ChangePasswordActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<UserRegistrationResponse> call = apiService.changePassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UserRegistrationResponse>() {
                @Override
                public void onResponse(Call<UserRegistrationResponse> call, Response<UserRegistrationResponse> response) {
                    if(response.isSuccessful()){
                        UserRegistrationResponse changePasswordResponse = response.body();
                        if(changePasswordResponse.getStatus()){
                            String message = changePasswordResponse.getMessage();
                            showOneButtonAlertDialog(message, getResources().getString(R.string.success),
                                    getResources().getString(R.string.ok), ChangePasswordActivity.this);
                        }
                        else {
                            String failureResponse = changePasswordResponse.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), ChangePasswordActivity.this);
                        }
                    }
                    else{
                        Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    if(dialog != null){
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<UserRegistrationResponse> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ChangePasswordActivity.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ChangePasswordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    if(dialog != null){
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private void showOneButtonAlertDialog(String descriptionStr, String titleStr, String buttonStr, Activity context){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        title.setText(titleStr);
        yes.setText(buttonStr);
        desc.setText(descriptionStr);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
                finish();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
