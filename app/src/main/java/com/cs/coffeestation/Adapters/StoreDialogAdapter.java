package com.cs.coffeestation.Adapters;


import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.Brands;
import com.cs.coffeestation.Models.MyAddress;
import com.cs.coffeestation.Models.StoresList;
import com.cs.coffeestation.R;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.support.constraint.Constraints.TAG;
public class StoreDialogAdapter extends RecyclerView.Adapter<StoreDialogAdapter.MyViewHolder> {
    private Context context;
    private int selectedPosition = 0;
    private ArrayList<Brands> storeArrayList = new ArrayList<>();
    private Activity activity;
    int pos = 0;

    public StoreDialogAdapter(Context context, ArrayList<Brands> storeArrayList, int pos, Activity activity){
        this.context = context;
        this.activity = activity;
        this.pos = pos;
        this.storeArrayList = storeArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dialog_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        holder.storename.setText(storeArrayList.get(pos).getBrands().get(position).getBranchnameEn());
        holder.storetype.setText(storeArrayList.get(pos).getBrands().get(position).getStoretype());
        holder.distance.setText(priceFormat.format(storeArrayList.get(pos).getBrands().get(position).getDistance())+" KM");
        holder.rating.setText(storeArrayList.get(pos).getBrands().get(position).getRating());

        if (storeArrayList.get(pos).getBrands().get(position).getStorestatus().equalsIgnoreCase("close")){
            holder.storestatus.setVisibility(View.VISIBLE);

            String openDateStr = storeArrayList.get(position).getBrands().get(pos).getStardatetime();
            openDateStr = openDateStr.replace("T", " ");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");
//
//        String tt = array[1];
            try {
                Date time = sdf.parse(openDateStr);
                openDateStr = sdf1.format(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.storestatus.setText("Next open at "+openDateStr);
        }
        else{
            if (storeArrayList.get(pos).getBrands().get(position).getDiscountamt()>0){
                holder.storestatus.setVisibility(View.VISIBLE);
                holder.storestatus.setText(""+storeArrayList.get(pos).getBrands().get(position).getDiscountamt()+"%");
            }
            else {
                holder.storestatus.setVisibility(View.GONE);
            }
            Log.d("TAG",""+storeArrayList.get(pos).getBrands().get(position).getDiscountamt());
        }
    }

    @Override
    public int getItemCount() {

        return storeArrayList.get(pos).getBrands().size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView storename, storetype, distance, rating,storestatus;
        public MyViewHolder(View itemView) {
            super(itemView);
            storename = (TextView) itemView.findViewById(R.id.storename);
            storetype = (TextView) itemView.findViewById(R.id.storetype);
            distance = (TextView) itemView.findViewById(R.id.distance);
            rating = (TextView) itemView.findViewById(R.id.rating);
            storestatus = (TextView)itemView.findViewById(R.id.storestatus);
        }
    }
}
