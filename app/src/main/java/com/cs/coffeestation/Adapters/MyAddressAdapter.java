package com.cs.coffeestation.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.coffeestation.Activities.AddAddressActivity;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.AddAddress;
import com.cs.coffeestation.Models.MyAddress;
import com.cs.coffeestation.R;
import com.cs.coffeestation.Rest.APIInterface;
import com.cs.coffeestation.Rest.ApiClient;
import com.cs.coffeestation.Utils.NetworkUtil;

import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.coffeestation.Activities.MyAddressActivity.ADD_ADDRESS_INTENT;
import static com.cs.coffeestation.Constants.TAG;

public class MyAddressAdapter extends RecyclerView.Adapter<MyAddressAdapter.MyViewHolder> {

    private Context context;
    private int selectedPosition = 0;
    private ArrayList<MyAddress.Data> addressArrayList = new ArrayList<>();
    private Activity activity;

    public MyAddressAdapter(Context context, ArrayList<MyAddress.Data> addressArrayList, Activity activity){
        this.context = context;
        this.activity = activity;
        this.addressArrayList = addressArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_my_address, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        MyAddress.Data myAddress = addressArrayList.get(position);
        holder.addressName.setText(myAddress.getAddressname());
        String address = myAddress.getHouseno() + ", "+ myAddress.getLandmark() + ", " + myAddress.getAddress();
        holder.addressBody.setText(address);

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                startAddressActivity();
            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                showTwoButtonAlertDialog(context.getResources().getString(R.string.delete_alert),
                        context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.yes),
                        context.getResources().getString(R.string.no), activity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView addressIcon;
        TextView addressName, addressBody;
        Button editButton, deleteButton;

        public MyViewHolder(View itemView) {
            super(itemView);
            addressIcon = (ImageView) itemView.findViewById(R.id.address_icon);
            addressName = (TextView) itemView.findViewById(R.id.address_name);
            addressBody = (TextView) itemView.findViewById(R.id.address_body);
            deleteButton = (Button) itemView.findViewById(R.id.address_delete_button);
            editButton = (Button) itemView.findViewById(R.id.address_edit_button);

            addressName.setBackgroundColor(context.getResources().getColor(R.color.white));
            addressBody.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
    }

    private class DeleteAddressApi extends AsyncTask<String, Integer, String> {

        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareDeleteAddressJson();
            dialog = new ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<AddAddress> call = apiService.addAddress(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<AddAddress>() {
                @Override
                public void onResponse(Call<AddAddress> call, Response<AddAddress> response) {
                    if(response.isSuccessful()){
                        AddAddress addressResponse = response.body();
                        try {
                            if(addressResponse.getStatus()){
                                addressArrayList.remove(selectedPosition);
                                notifyDataSetChanged();
                            }
                            else {
                                String failureResponse = addressResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.error),
                                        context.getResources().getString(R.string.ok), activity);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddAddress> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(dialog != null){
                dialog.dismiss();
            }
        }
    }

    private String prepareDeleteAddressJson(){
        JSONObject addressObj = new JSONObject();

        try {
            addressObj.put("AddressId", addressArrayList.get(selectedPosition).getAddressid());
            addressObj.put("UserId", "4");
//            addressObj.put("HouseNo", addressArrayList.get(selectedPosition).getHouseno());
//            addressObj.put("HouseName", addressArrayList.get(selectedPosition).getAddressname());
//            addressObj.put("LandMark", addressArrayList.get(selectedPosition).getLandmark());
//            addressObj.put("AddressType", "Home");
//            addressObj.put("Address", strAddress);
//            addressObj.put("Latitude", lat);
//            addressObj.put("Longitude", longi);
            addressObj.put("IsActive", false);

            Log.d(TAG, "prepareDeleteAddressJson: "+addressObj.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return addressObj.toString();
    }

    private void startAddressActivity(){
        Intent intent = new Intent(activity, AddAddressActivity.class);
        intent.putExtra("edit", true);
        intent.putExtra("AddressId",addressArrayList.get(selectedPosition).getAddressid());
        intent.putExtra("HouseNo", addressArrayList.get(selectedPosition).getHouseno());
        intent.putExtra("HouseName", addressArrayList.get(selectedPosition).getAddressname());
        intent.putExtra("LandMark", addressArrayList.get(selectedPosition).getLandmark());
        intent.putExtra("AddressType", "Home");
        intent.putExtra("Address", addressArrayList.get(selectedPosition).getAddress());
        intent.putExtra("Latitude",addressArrayList.get(selectedPosition).getLatitude());
        intent.putExtra("Longitude",addressArrayList.get(selectedPosition).getLongitude());
        ((Activity) context).startActivityForResult(intent, ADD_ADDRESS_INTENT);
    }

    public void showTwoButtonAlertDialog(String descriptionStr, String titleStr, String positiveStr, String negativeStr, final Activity context){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        title.setText(titleStr);
        yes.setText(positiveStr);
        no.setText(negativeStr);
        desc.setText(descriptionStr);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
                String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new DeleteAddressApi().execute();
                }
                else{
                    Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
