package com.cs.coffeestation.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.StoresList;
import com.cs.coffeestation.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by PULi on 19-2-2019.
 */
public class PViewallAdapter extends RecyclerView.Adapter<PViewallAdapter.MyViewHolder>{

    private Context context;
    //    private int selectedPosition = 0;
    ArrayList<StoresList.StoresDetails> storesArrayList;
    private Activity activity;
    public static final String TAG = "TAG";
    int pos = 0;

    public PViewallAdapter(Context context, ArrayList<StoresList.StoresDetails> storesArrayList, Activity activity){
        this.context = context;
        this.activity = activity;
        this.storesArrayList = storesArrayList;
    }


    @Override
    public PViewallAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.storemenu, parent, false);
//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new PViewallAdapter.MyViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull PViewallAdapter.MyViewHolder holder, int position) {

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        StoresList.StoresDetails storesDetails = storesArrayList.get(position);
        holder.store_type.setText(storesDetails.getStoretype());
        holder.store_name.setText(storesDetails.getBrandnameEn());
        holder.store_distance.setText(priceFormat.format(storesDetails.getDistance())+" KM");
        holder.store_delivery_time.setText(storesDetails.getRating());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.IMAGE_URL+( storesDetails).getStoreimageEn())
                .into(holder.store_image);

        Glide.with(context)
                .load(Constants.IMAGE_URL+storesDetails.getStorelogoEn())
                .into(holder.store_logo);
    }
    @Override
    public int getItemCount() {
        Log.d(TAG, "Arrylistsize"+storesArrayList.size());
        return storesArrayList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView store_type, store_name, store_delivery_time, store_distance;
        ImageView store_image, store_logo;
//        LinearLayout rating_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            store_name = (TextView) itemView.findViewById(R.id.store_name);
            store_type = (TextView) itemView.findViewById(R.id.store_type);
            store_logo = (ImageView) itemView.findViewById(R.id.store_icon);
            store_distance = (TextView) itemView.findViewById(R.id.store_distance);
            store_delivery_time = (TextView) itemView.findViewById(R.id.store_delivery_time);
            store_image = (ImageView) itemView.findViewById(R.id.store_image);
            store_logo = (ImageView) itemView.findViewById(R.id.store_icon);

//            Changing backgroundcolor after shimmer effect
            store_name.setBackgroundColor(Color.TRANSPARENT);
            store_type.setBackgroundColor(Color.TRANSPARENT);
            store_logo.setBackgroundColor(Color.TRANSPARENT);
            store_delivery_time.setBackgroundColor(Color.TRANSPARENT);
//                    itemView.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            SearchFragment.searchTex = storesArrayList.get(getAdapterPosition()).gets();
//                            Intent intent = new Intent("Searchtream");
//                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
//                        }
//                    });
        }
    }
}