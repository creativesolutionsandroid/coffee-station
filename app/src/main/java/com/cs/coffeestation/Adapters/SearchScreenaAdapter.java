package com.cs.coffeestation.Adapters;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.coffeestation.Constants;
//import com.cs.coffeestation.Dialogs.StoresListDialog;
import com.cs.coffeestation.Fragments.SearchFragment;
import com.cs.coffeestation.Models.Brands;
import com.cs.coffeestation.Models.StoresList;
import com.cs.coffeestation.R;
import java.text.DecimalFormat;
import java.util.ArrayList;
import static com.cs.coffeestation.Fragments.SearchFragment.searchTex;

public class
SearchScreenaAdapter extends RecyclerView.Adapter<SearchScreenaAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    //    private int selectedPosition = 0;
    ArrayList<StoresList.StoreCategories> storesArrayList;

    private Activity activity;

    public SearchScreenaAdapter(Context context, ArrayList<StoresList.StoreCategories> storesArrayList, Activity activity){
        this.context = context;
        this.activity = activity;
        this.storesArrayList = storesArrayList;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_list1, parent, false);

        return new MyViewHolder(itemView);
    }

  public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");

        StoresList.StoreCategories storeDetails = storesArrayList.get(position);
        holder.store_delivery_time.setText(storeDetails.getCatetgorynameEn());


        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.IMAGE_URL+storesArrayList.get(position).getImageEn())
                .into(holder.store_image);

    }

    @Override
    public int getItemCount() {
        return storesArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView  store_delivery_time;
        ImageView store_image;

        public MyViewHolder(View itemView) {
            super(itemView);

           store_delivery_time = (TextView) itemView.findViewById(R.id.store_delivery_time);
            store_image = (ImageView) itemView.findViewById(R.id.store_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    searchTex = storesArrayList.get(getAdapterPosition()).getCatetgorynameEn();
                    Intent intent = new Intent("Searchtream");
//                    Bundle b = new Bundle();
//                    b.putInt("id", storesArrayList.get(getAdapterPosition()).getStorecatid());
                    intent.putExtra("id", storesArrayList.get(getAdapterPosition()).getStorecatid());
                    intent.putExtra("typename", storesArrayList.get(getAdapterPosition()).getCatetgorynameEn());
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                    Log.d(TAG, "onClick "+ storesArrayList.get(getAdapterPosition()).getStorecatid());
                }
            });

        }
    }
}
