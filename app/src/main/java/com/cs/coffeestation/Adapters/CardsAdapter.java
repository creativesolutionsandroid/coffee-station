package com.cs.coffeestation.Adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.coffeestation.R;

public class CardsAdapter extends RecyclerView.Adapter<com.cs.coffeestation.Adapters.CardsAdapter.MyViewHolder>{

    @NonNull
    @Override
    public CardsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardadapter, parent, false);
//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new CardsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CardsAdapter.MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView store_type, store_name, store_delivery_time, store_distance;
        ImageView store_image, store_logo;
//        LinearLayout rating_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            store_name = (TextView) itemView.findViewById(R.id.store_name);
            store_type = (TextView) itemView.findViewById(R.id.store_type);
            store_logo = (ImageView) itemView.findViewById(R.id.store_icon);
            store_distance = (TextView) itemView.findViewById(R.id.store_distance);
            store_delivery_time = (TextView) itemView.findViewById(R.id.store_delivery_time);
            store_image = (ImageView) itemView.findViewById(R.id.store_image);
            store_logo = (ImageView) itemView.findViewById(R.id.store_icon);

//            Changing backgroundcolor after shimmer effect
            store_name.setBackgroundColor(Color.TRANSPARENT);
            store_type.setBackgroundColor(Color.TRANSPARENT);
            store_logo.setBackgroundColor(Color.TRANSPARENT);
            store_delivery_time.setBackgroundColor(Color.TRANSPARENT);
//                    itemView.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            SearchFragment.searchTex = storesArrayList.get(getAdapterPosition()).gets();
//                            Intent intent = new Intent("Searchtream");
//                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
//                        }
//                    });
        }
    }
}
