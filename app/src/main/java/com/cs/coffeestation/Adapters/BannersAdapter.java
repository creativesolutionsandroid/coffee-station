package com.cs.coffeestation.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.StoresList;
import com.cs.coffeestation.R;

import java.util.ArrayList;


public class BannersAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<StoresList.BannerResult> bannerList;
    Context context;

    public BannersAdapter(Context context, ArrayList<StoresList.BannerResult> bannerList) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bannerList = bannerList;
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 1.0f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return bannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((android.support.v7.widget.CardView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_popular_stores, container, false);

        ImageView store_image = (ImageView) itemView.findViewById(R.id.store_image);
        ImageView store_logo = (ImageView) itemView.findViewById(R.id.store_icon);
        TextView store_type = (TextView) itemView.findViewById(R.id.store_type);
        TextView store_delivery_time = (TextView) itemView.findViewById(R.id.store_delivery_time);
        TextView store_name = (TextView) itemView.findViewById(R.id.store_name);
        TextView store_distance = (TextView) itemView.findViewById(R.id.store_distance);

        store_name.setBackgroundColor(Color.TRANSPARENT);
        store_type.setBackgroundColor(Color.TRANSPARENT);
        store_logo.setBackgroundColor(Color.TRANSPARENT);
        store_delivery_time.setBackgroundColor(Color.TRANSPARENT);

////        store_type.setText(bannerList.get(position).get());
//        store_name.setText(bannerList.get(position).getBannernameEn());
//        store_logo.setImageDrawable(context.getResources().getDrawable(R.drawable.dr_cafe_logo));

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.IMAGE_URL+bannerList.get(position).getBannerimage())
                .into(store_image);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((android.support.v7.widget.CardView) object);
    }
}