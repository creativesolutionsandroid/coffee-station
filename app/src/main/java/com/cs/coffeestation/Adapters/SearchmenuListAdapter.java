package com.cs.coffeestation.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Dialogs.SearchListDialog;
import com.cs.coffeestation.Models.Brands;
import com.cs.coffeestation.Models.StoresList;
import com.cs.coffeestation.R;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SearchmenuListAdapter extends RecyclerView.Adapter<SearchmenuListAdapter.MyViewHolder> {

    private Context context;
    //    private int selectedPosition = 0;
    ArrayList<StoresList.StoresDetails> storesArrayList;
    ArrayList<Brands> brandslist;
    private Activity activity;
    public static final String TAG = "TAG";
    int pos = 0;

    public SearchmenuListAdapter(Context context, ArrayList<StoresList.StoresDetails> storesArrayList, Activity activity){
        this.context = context;
        this.activity = activity;
        this.storesArrayList = storesArrayList;
    }


 @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.storemenu, parent, false);
//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

    final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        StoresList.StoresDetails storesDetails = storesArrayList.get(position);
        holder.store_type.setText(storesDetails.getStoretype());
        holder.store_name.setText(storesDetails.getBrandnameEn());
        holder.store_distance.setText(priceFormat.format(storesDetails.getDistance())+" KM");
        holder.store_delivery_time.setText(storesDetails.getRating());
//        holder.disscount.setText(storesDetails.getDiscountamt()+"%");

        holder.storeopen.setText(storesDetails.getStardatetime());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        String openDateStr = storesArrayList.get(position).getStardatetime();
        openDateStr = openDateStr.replace("T", " ");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");

//        String tt = array[1];
        try {
            Date time = sdf.parse(openDateStr);
            openDateStr = sdf1.format(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (storesArrayList.get(position).getDiscountamt()>0){
           holder.disscount.setVisibility(View.VISIBLE);
            holder.disscount.setText(storesDetails.getDiscountamt()+"%");
       }
        else {
            holder.disscount.setVisibility(View.GONE);
       }

       if (storesArrayList.get(position).getStorestatus().equalsIgnoreCase("Close")){

           holder.storeopen.setVisibility(View.VISIBLE);
           holder.storeopen.setText("Next open at "+openDateStr);
        }
        else {
           holder.storeopen.setVisibility(View.GONE);
       }

        Glide.with(context)
                .load(Constants.IMAGE_URL+( storesDetails).getStoreimageEn())
                .into(holder.store_image);

        Glide.with(context)
                .load(Constants.IMAGE_URL+storesDetails.getStorelogoEn())
                .into(holder.store_logo);
    }
    @Override
    public int getItemCount() {
        Log.d(TAG, "Arrylistsize"+storesArrayList.size());
        return storesArrayList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView store_type, store_name, store_delivery_time, store_distance,storeopen,disscount;
        ImageView store_image, store_logo;
//        LinearLayout rating_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            store_name = (TextView) itemView.findViewById(R.id.store_name);
            store_type = (TextView) itemView.findViewById(R.id.store_type);
            store_logo = (ImageView) itemView.findViewById(R.id.store_icon);
            store_distance = (TextView) itemView.findViewById(R.id.store_distance);
            store_delivery_time = (TextView) itemView.findViewById(R.id.store_delivery_time);
            store_image = (ImageView) itemView.findViewById(R.id.store_image);
            store_logo = (ImageView) itemView.findViewById(R.id.store_icon);
            disscount = (TextView) itemView.findViewById(R.id.disscount1);
            storeopen = (TextView) itemView.findViewById(R.id.storeopen);

//            Changing backgroundcolor after shimmer effect
            store_name.setBackgroundColor(Color.TRANSPARENT);
            store_type.setBackgroundColor(Color.TRANSPARENT);
            store_logo.setBackgroundColor(Color.TRANSPARENT);
            store_delivery_time.setBackgroundColor(Color.TRANSPARENT);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle args = new Bundle();
//                    Log.d("TAG", "storeArrayList "+storesArrayList.size());
//                    Log.d("TAG", "pos1"+getAdapterPosition());
                    args.putSerializable("array1", storesArrayList);
                    args.putInt("pos1", getAdapterPosition());

                    final SearchListDialog newFragment = SearchListDialog.newInstance();
                    newFragment.setCancelable(true);
                    newFragment.setArguments(args);
                    newFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), "store1");
//                    Pair<View, String> p1 = Pair.create((View)store_image, "store_image");
//                    Pair<View, String> p2 = Pair.create((View)store_name, "store_name");
//                    Pair<View, String> p3= Pair.create((View)store_type, "store_type");
//                    Intent intent = new Intent(context, StoreMenuActivity.class);
//                    intent.putExtra("array", storesArrayList.get(getPosition()));
//                    ActivityOptionsCompat options = ActivityOptionsCompat.
//                            makeSceneTransitionAnimation(activity, p1, p2, p3);
//                    context.startActivity(intent, options.toBundle());
                }
            });

        }
        }
    }
