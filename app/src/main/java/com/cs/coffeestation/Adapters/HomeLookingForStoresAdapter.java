package com.cs.coffeestation.Adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.coffeestation.Activities.ViewallScreenActivity;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Fragments.SearchFragment;
import com.cs.coffeestation.Models.StoresList;
import com.cs.coffeestation.R;
import java.util.ArrayList;
import static android.support.constraint.Constraints.TAG;
import static com.cs.coffeestation.Fragments.SearchFragment.searchTex;
public class HomeLookingForStoresAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<StoresList.StoreCategories> bannerList;
    Context context;

 public HomeLookingForStoresAdapter(Context context, ArrayList<StoresList.StoreCategories> bannerList) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bannerList = bannerList;
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 2.5f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return bannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((android.support.v7.widget.CardView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_looking_for_stores, container, false);

        ImageView store_image = (ImageView) itemView.findViewById(R.id.store_image);
        TextView store_type = (TextView) itemView.findViewById(R.id.store_delivery_time);
//        store_image.setImageDrawable(context.getResources().getDrawable((R.drawable.menu_cup)));
        store_type.setText(bannerList.get(position).getCatetgorynameEn());

        Glide.with(context)
                .load(Constants.IMAGE_URL+bannerList.get(position).getImageEn())
                .into(store_image);

//        store_image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                searchTex = bannerList.get(position).getCatetgorynameEn();
//                Bundle args = new Bundle();
//                args.putString("category_id",bannerList.get(position).getCatetgorynameEn());
//                FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
////                FragmentManager fragmentManager = ;
//                Fragment searchFragment = new SearchFragment();
//                searchFragment.setArguments(args);
//                fragmentManager.beginTransaction().replace(R.id.fragment_layout, searchFragment).commit();
//
////                Log.d(TAG, "onClick "+ bannerList.get(position).getStorecatid());
//            }
//        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((android.support.v7.widget.CardView) object);
    }

}
