package com.cs.coffeestation.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.coffeestation.Models.MenuItems;
import com.cs.coffeestation.Models.MyAddress;
import com.cs.coffeestation.R;

import java.util.ArrayList;

public class MenuItemsAdapter extends RecyclerView.Adapter<MenuItemsAdapter.MyViewHolder> {

    private Context context;
    private int expandedPosition = 0;
    private ArrayList<MenuItems.Data> itemsArrayList = new ArrayList<>();
    private Activity activity;
    private ArrayList<String> headers = new ArrayList<>();
    private ArrayList<String> displayedHeaders = new ArrayList<>();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    public MenuItemsAdapter(Context context, ArrayList<MenuItems.Data> itemsArrayList, ArrayList<String> headers, Activity activity){
        this.context = context;
        this.activity = activity;
        this.headers = headers;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_menu_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        MenuItems.Data data = itemsArrayList.get(position);

        if(data.isHeader()){
            holder.header.setText(data.getMcatEn());
            holder.header.setVisibility(View.VISIBLE);
            holder.subCategoryLayout.setVisibility(View.GONE);
            holder.expandableLayout.setVisibility(View.GONE);
        }
        else {
            holder.header.setVisibility(View.GONE);
            holder.subCategoryLayout.setVisibility(View.VISIBLE);
            holder.expandableLayout.setVisibility(View.VISIBLE);

            holder.subCategoryName.setText(data.getScat().get(position).getScatEn());

            ArrayList<MenuItems.ITM> subArray = new ArrayList<>();
            subArray = data.getScat().get(position).getSec().get(0).getItm();
            holder.subCategoryCount.setText(subArray.size() + " items");

            holder.itemsLayout.removeAllViews();
            for (int i = 0; i < subArray.size(); i++) {
                LayoutInflater inflater = LayoutInflater.from(context);
                View v = inflater.inflate(R.layout.list_expandable_menu_items, null);
                TextView addtionalsName = (TextView) v.findViewById(R.id.item_name);
                TextView addtionalsPrice = (TextView) v.findViewById(R.id.item_price);
                addtionalsName.setText(subArray.get(i).getItemnameEn());
                addtionalsPrice.setText(subArray.get(position).getIprc().get(0).getItemprice() + ".00 SR");
                holder.itemsLayout.addView(v);
            }

            if (expandedPosition == position) {
                holder.expandableLayout.setVisibility(View.VISIBLE);
            } else {
                holder.expandableLayout.setVisibility(View.GONE);
            }

            holder.subCategoryLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.expandableLayout.getVisibility() == View.VISIBLE) {
                        holder.expandableLayout.setVisibility(View.GONE);
                    } else {
                        holder.expandableLayout.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (itemsArrayList.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView downArrow;
        TextView subCategoryName, subCategoryCount, header;
        Button addButton;
        RelativeLayout subCategoryLayout;
        LinearLayout expandableLayout, itemsLayout;
        View shortLine;

        public MyViewHolder(View itemView) {
            super(itemView);
            downArrow = (ImageView) itemView.findViewById(R.id.menu_arrow);
            header = (TextView) itemView.findViewById(R.id.section_header);
            subCategoryName = (TextView) itemView.findViewById(R.id.sub_category_name);
            subCategoryCount = (TextView) itemView.findViewById(R.id.sub_category_count);
            addButton = (Button) itemView.findViewById(R.id.add_button);
            subCategoryLayout = (RelativeLayout) itemView.findViewById(R.id.sub_category_layout);
            expandableLayout = (LinearLayout) itemView.findViewById(R.id.expandable_layout);
            itemsLayout = (LinearLayout) itemView.findViewById(R.id.items_layout);
            shortLine = (View) itemView.findViewById(R.id.line);
        }
    }
}
