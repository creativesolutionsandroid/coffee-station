package com.cs.coffeestation.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.coffeestation.Activities.AddAddressActivity;
import com.cs.coffeestation.Constants;
import com.cs.coffeestation.Models.AddAddress;
import com.cs.coffeestation.Models.MyAddress;
import com.cs.coffeestation.R;
import com.cs.coffeestation.Rest.APIInterface;
import com.cs.coffeestation.Rest.ApiClient;
import com.cs.coffeestation.Utils.NetworkUtil;

import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.cs.coffeestation.Activities.MyAddressActivity.ADD_ADDRESS_INTENT;
import static com.cs.coffeestation.Constants.TAG;

public class ChangeAddressAdapter extends RecyclerView.Adapter<ChangeAddressAdapter.MyViewHolder> {

    private Context context;
    private int selectedPosition = 0;
    private ArrayList<MyAddress.Data> addressArrayList = new ArrayList<>();
    private Activity activity;

    public ChangeAddressAdapter(Context context, ArrayList<MyAddress.Data> addressArrayList, Activity activity){
        this.context = context;
        this.activity = activity;
        this.addressArrayList = addressArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_change_address, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        MyAddress.Data myAddress = addressArrayList.get(position);
        holder.addressName.setText(myAddress.getAddressname());
        String address = myAddress.getHouseno() + ", "+ myAddress.getLandmark() + ", " + myAddress.getAddress();
        holder.addressBody.setText(address);

        holder.addressLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("lat", addressArrayList.get(position).getLatitude());
                intent.putExtra("longi", addressArrayList.get(position).getLongitude());
                activity.setResult(RESULT_OK, intent);
                activity.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView addressIcon;
        TextView addressName, addressBody;
        RelativeLayout addressLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            addressIcon = (ImageView) itemView.findViewById(R.id.address_icon);
            addressName = (TextView) itemView.findViewById(R.id.address_name);
            addressBody = (TextView) itemView.findViewById(R.id.address_body);
            addressLayout = (RelativeLayout) itemView.findViewById(R.id.address_layout);

            addressName.setBackgroundColor(context.getResources().getColor(R.color.white));
            addressBody.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
    }
}
