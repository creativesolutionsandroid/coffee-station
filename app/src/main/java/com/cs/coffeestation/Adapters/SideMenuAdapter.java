package com.cs.coffeestation.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.coffeestation.R;

public class SideMenuAdapter extends ArrayAdapter<String> {

    Context context;
    String[] menuItems;
    Integer[] menuImages;
    int layoutResourceId;
    int selectedPosition;

    public SideMenuAdapter(Context context, int layoutResourceId, String[] menuItems, Integer[] menuImages, int selectedPosition){
        super(context, layoutResourceId, menuItems);
        this.context = context;
        this.menuItems = menuItems;
        this.menuImages = menuImages;
        this.layoutResourceId = layoutResourceId;
        this.selectedPosition = selectedPosition;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        if (row == null) {
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();

            holder.textTitle = (TextView) row.findViewById(R.id.menu_title);
            holder.menuImage = (ImageView) row.findViewById(R.id.menu_image);
            holder.textLanguage = (TextView) row.findViewById(R.id.menu_subtitle);
            holder.menuItemLayout = (LinearLayout) row.findViewById(R.id.menu_item_layout);
            holder.dividerLine = (View) row.findViewById(R.id.divider_line);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        holder.textTitle.setText(menuItems[position]);
        holder.menuImage.setImageDrawable(context.getResources().getDrawable(menuImages[position]));

        if(position == 6){
            holder.textLanguage.setVisibility(View.VISIBLE);
        }
        else{
            holder.textLanguage.setVisibility(View.INVISIBLE);
        }

        if(selectedPosition == position){
            holder.menuItemLayout.setBackgroundColor(context.getResources().getColor(R.color.side_menu_item_selected_color));
            holder.dividerLine.setBackgroundColor(context.getResources().getColor(R.color.line_status_bar_color));
        }
        else{
            holder.menuItemLayout.setBackgroundColor(context.getResources().getColor(R.color.side_menu_brown_color));
            holder.dividerLine.setBackgroundColor(Color.parseColor("#33FFFFFF"));
        }
        return row;
    }

    static class ViewHolder {
        TextView textTitle, textLanguage;
        ImageView menuImage;
        LinearLayout menuItemLayout;
        View dividerLine;
    }
}
