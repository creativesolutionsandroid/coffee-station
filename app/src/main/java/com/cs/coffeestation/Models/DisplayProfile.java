package com.cs.coffeestation.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DisplayProfile {

    @Expose
    @SerializedName("Success")
    private Success success;

    @Expose
    @SerializedName("Failure")
    private String failure;

    public String getFailure() {
        return failure;
    }

    public void setFailure(String failure) {
        this.failure = failure;
    }

    public Success getSuccess() {
        return success;
    }

    public void setSuccess(Success success) {
        this.success = success;
    }

    public static class Success {
        @Expose
        @SerializedName("Email")
        private String email;
        @Expose
        @SerializedName("Mobile")
        private String mobile;
        @Expose
        @SerializedName("Gender")
        private String gender;
        @Expose
        @SerializedName("NickName")
        private String nickname;
        @Expose
        @SerializedName("FamilyName")
        private String familyname;
        @Expose
        @SerializedName("FullName")
        private String fullname;
        @Expose
        @SerializedName("UserId")
        private int userid;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getFamilyname() {
            return familyname;
        }

        public void setFamilyname(String familyname) {
            this.familyname = familyname;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }
    }
}
