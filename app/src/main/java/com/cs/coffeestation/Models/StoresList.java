package com.cs.coffeestation.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;


public class StoresList implements Serializable {


    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("BannerResult")
        private ArrayList<BannerResult> bannerresult;
        @Expose
        @SerializedName("StoreCategories")
        private  ArrayList<StoreCategories> storecategories;
        @Expose
        @SerializedName("StoresDetails")
        private  ArrayList<StoresDetails> storesdetails;

        public ArrayList<BannerResult> getBannerresult() {
            return bannerresult;
        }

        public void setBannerresult(ArrayList<BannerResult> bannerresult) {
            this.bannerresult = bannerresult;
        }

        public ArrayList<StoreCategories> getStorecategories() {
            return storecategories;
        }

        public void setStorecategories(ArrayList<StoreCategories> storecategories) {
            this.storecategories = storecategories;
        }

        public ArrayList<StoresDetails> getStoresdetails() {
            return storesdetails;
        }

        public void setStoresdetails(ArrayList<StoresDetails> storesdetails) {
            this.storesdetails = storesdetails;
        }
    }
    public static class BannerResult implements Serializable {
        @Expose
        @SerializedName("StoreDetails")
        private  ArrayList<StoreDetails> storedetails;
        @Expose
        @SerializedName("BannerImage")
        private String bannerimage;
        @Expose
        @SerializedName("BannerName_Ar")
        private String bannernameAr;
        @Expose
        @SerializedName("BannerName_En")
        private String bannernameEn;

        public ArrayList<StoreDetails> getStoredetails() {
            return storedetails;
        }

        public void setStoredetails(ArrayList<StoreDetails> storedetails) {
            this.storedetails = storedetails;
        }

        public String getBannerimage() {
            return bannerimage;
        }

        public void setBannerimage(String bannerimage) {
            this.bannerimage = bannerimage;
        }

        public String getBannernameAr() {
            return bannernameAr;
        }

        public void setBannernameAr(String bannernameAr) {
            this.bannernameAr = bannernameAr;
        }

        public String getBannernameEn() {
            return bannernameEn;
        }

        public void setBannernameEn(String bannernameEn) {
            this.bannernameEn = bannernameEn;
        }
    }

    public static class StoreDetails implements Serializable {
        @Expose
        @SerializedName("DiscountAmt")
        private int discountamt;
        @Expose
        @SerializedName("PaymentType")
        private String paymenttype;
        @Expose
        @SerializedName("StoreType")
        private String storetype;
        @Expose
        @SerializedName("OrderTypes")
        private String ordertypes;
        @Expose
        @SerializedName("RatingCount")
        private int ratingcount;
        @Expose
        @SerializedName("IsNew")
        private boolean isnew;
        @Expose
        @SerializedName("Popular")
        private boolean popular;
        @Expose
        @SerializedName("IsPromoted")
        private boolean ispromoted;
        @Expose
        @SerializedName("AvgPreparationTime")
        private int avgpreparationtime;
        @Expose
        @SerializedName("DeliveryCharges")
        private int deliverycharges;
        @Expose
        @SerializedName("MinOrderCharges")
        private int minordercharges;
        @Expose
        @SerializedName("Longitude")
        private double longitude;
        @Expose
        @SerializedName("Latitude")
        private double latitude;
        @Expose
        @SerializedName("Rating")
        private String rating;
        @Expose
        @SerializedName("Distance")
        private double distance;
        @Expose
        @SerializedName("StoreImage_En")
        private String storeimageEn;
        @Expose
        @SerializedName("StoreLogo_En")
        private String storelogoEn;
        @Expose
        @SerializedName("StoreStatus")
        private String storestatus;
        @Expose
        @SerializedName("TakeAwayDistance")
        private int takeawaydistance;
        @Expose
        @SerializedName("DeliveryDistance")
        private int deliverydistance;
        @Expose
        @SerializedName("EmailId")
        private String emailid;
        @Expose
        @SerializedName("MobileNo")
        private String mobileno;
        @Expose
        @SerializedName("TelephoneNo")
        private String telephoneno;
        @Expose
        @SerializedName("Address")
        private String address;
        @Expose
        @SerializedName("BranchCode")
        private String branchcode;
        @Expose
        @SerializedName("CurrentDateTime")
        private String currentdatetime;
        @Expose
        @SerializedName("EndDateTime")
        private String enddatetime;
        @Expose
        @SerializedName("StarDateTime")
        private String stardatetime;
        @Expose
        @SerializedName("BranchDescription_Ar")
        private String branchdescriptionAr;
        @Expose
        @SerializedName("BranchDescription_En")
        private String branchdescriptionEn;
        @Expose
        @SerializedName("BrandName_Ar")
        private String brandnameAr;
        @Expose
        @SerializedName("BrandName_En")
        private String brandnameEn;
        @Expose
        @SerializedName("BrandId")
        private int brandid;
        @Expose
        @SerializedName("BranchName_Ar")
        private String branchnameAr;
        @Expose
        @SerializedName("BranchName_En")
        private String branchnameEn;
        @Expose
        @SerializedName("BranchId")
        private int branchid;
        @Expose
        @SerializedName("WeekNo")
        private int weekno;
        @Expose
        @SerializedName("Id")
        private int id;

        public int getDiscountamt() {
            return discountamt;
        }

        public void setDiscountamt(int discountamt) {
            this.discountamt = discountamt;
        }

        public String getPaymenttype() {
            return paymenttype;
        }

        public void setPaymenttype(String paymenttype) {
            this.paymenttype = paymenttype;
        }

        public String getStoretype() {
            return storetype;
        }

        public void setStoretype(String storetype) {
            this.storetype = storetype;
        }

        public String getOrdertypes() {
            return ordertypes;
        }

        public void setOrdertypes(String ordertypes) {
            this.ordertypes = ordertypes;
        }

        public int getRatingcount() {
            return ratingcount;
        }

        public void setRatingcount(int ratingcount) {
            this.ratingcount = ratingcount;
        }

        public boolean isIsnew() {
            return isnew;
        }

        public void setIsnew(boolean isnew) {
            this.isnew = isnew;
        }

        public boolean isPopular() {
            return popular;
        }

        public void setPopular(boolean popular) {
            this.popular = popular;
        }

        public boolean isIspromoted() {
            return ispromoted;
        }

        public void setIspromoted(boolean ispromoted) {
            this.ispromoted = ispromoted;
        }

        public int getAvgpreparationtime() {
            return avgpreparationtime;
        }

        public void setAvgpreparationtime(int avgpreparationtime) {
            this.avgpreparationtime = avgpreparationtime;
        }

        public int getDeliverycharges() {
            return deliverycharges;
        }

        public void setDeliverycharges(int deliverycharges) {
            this.deliverycharges = deliverycharges;
        }

        public int getMinordercharges() {
            return minordercharges;
        }

        public void setMinordercharges(int minordercharges) {
            this.minordercharges = minordercharges;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public String getStoreimageEn() {
            return storeimageEn;
        }

        public void setStoreimageEn(String storeimageEn) {
            this.storeimageEn = storeimageEn;
        }

        public String getStorelogoEn() {
            return storelogoEn;
        }

        public void setStorelogoEn(String storelogoEn) {
            this.storelogoEn = storelogoEn;
        }

        public String getStorestatus() {
            return storestatus;
        }

        public void setStorestatus(String storestatus) {
            this.storestatus = storestatus;
        }

        public int getTakeawaydistance() {
            return takeawaydistance;
        }

        public void setTakeawaydistance(int takeawaydistance) {
            this.takeawaydistance = takeawaydistance;
        }

        public int getDeliverydistance() {
            return deliverydistance;
        }

        public void setDeliverydistance(int deliverydistance) {
            this.deliverydistance = deliverydistance;
        }

        public String getEmailid() {
            return emailid;
        }

        public void setEmailid(String emailid) {
            this.emailid = emailid;
        }

        public String getMobileno() {
            return mobileno;
        }

        public void setMobileno(String mobileno) {
            this.mobileno = mobileno;
        }

        public String getTelephoneno() {
            return telephoneno;
        }

        public void setTelephoneno(String telephoneno) {
            this.telephoneno = telephoneno;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getBranchcode() {
            return branchcode;
        }

        public void setBranchcode(String branchcode) {
            this.branchcode = branchcode;
        }

        public String getCurrentdatetime() {
            return currentdatetime;
        }

        public void setCurrentdatetime(String currentdatetime) {
            this.currentdatetime = currentdatetime;
        }

        public String getEnddatetime() {
            return enddatetime;
        }

        public void setEnddatetime(String enddatetime) {
            this.enddatetime = enddatetime;
        }

        public String getStardatetime() {
            return stardatetime;
        }

        public void setStardatetime(String stardatetime) {
            this.stardatetime = stardatetime;
        }

        public String getBranchdescriptionAr() {
            return branchdescriptionAr;
        }

        public void setBranchdescriptionAr(String branchdescriptionAr) {
            this.branchdescriptionAr = branchdescriptionAr;
        }

        public String getBranchdescriptionEn() {
            return branchdescriptionEn;
        }

        public void setBranchdescriptionEn(String branchdescriptionEn) {
            this.branchdescriptionEn = branchdescriptionEn;
        }

        public String getBrandnameAr() {
            return brandnameAr;
        }

        public void setBrandnameAr(String brandnameAr) {
            this.brandnameAr = brandnameAr;
        }

        public String getBrandnameEn() {
            return brandnameEn;
        }

        public void setBrandnameEn(String brandnameEn) {
            this.brandnameEn = brandnameEn;
        }

        public int getBrandid() {
            return brandid;
        }

        public void setBrandid(int brandid) {
            this.brandid = brandid;
        }

        public String getBranchnameAr() {
            return branchnameAr;
        }

        public void setBranchnameAr(String branchnameAr) {
            this.branchnameAr = branchnameAr;
        }

        public String getBranchnameEn() {
            return branchnameEn;
        }

        public void setBranchnameEn(String branchnameEn) {
            this.branchnameEn = branchnameEn;
        }

        public int getBranchid() {
            return branchid;
        }

        public void setBranchid(int branchid) {
            this.branchid = branchid;
        }

        public int getWeekno() {
            return weekno;
        }

        public void setWeekno(int weekno) {
            this.weekno = weekno;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class StoreCategories implements Serializable {
        @Expose
        @SerializedName("BranchId")
        private int branchid;
        @Expose
        @SerializedName("Longitude")
        private int longitude;
        @Expose
        @SerializedName("Latitude")
        private int latitude;
        @Expose
        @SerializedName("Image_Ar")
        private String imageAr;
        @Expose
        @SerializedName("Image_En")
        private String imageEn;
        @Expose
        @SerializedName("Description_Ar")
        private String descriptionAr;
        @Expose
        @SerializedName("Description_En")
        private String descriptionEn;
        @Expose
        @SerializedName("CatetgoryName_Ar")
        private String catetgorynameAr;
        @Expose
        @SerializedName("CatetgoryName_En")
        private String catetgorynameEn;
        @Expose
        @SerializedName("StoreCatId")
        private int storecatid;

        public int getBranchid() {
            return branchid;
        }

        public void setBranchid(int branchid) {
            this.branchid = branchid;
        }

        public int getLongitude() {
            return longitude;
        }

        public void setLongitude(int longitude) {
            this.longitude = longitude;
        }

        public int getLatitude() {
            return latitude;
        }

        public void setLatitude(int latitude) {
            this.latitude = latitude;
        }

        public String getImageAr() {
            return imageAr;
        }

        public void setImageAr(String imageAr) {
            this.imageAr = imageAr;
        }

        public String getImageEn() {
            return imageEn;
        }

        public void setImageEn(String imageEn) {
            this.imageEn = imageEn;
        }

        public String getDescriptionAr() {
            return descriptionAr;
        }

        public void setDescriptionAr(String descriptionAr) {
            this.descriptionAr = descriptionAr;
        }

        public String getDescriptionEn() {
            return descriptionEn;
        }

        public void setDescriptionEn(String descriptionEn) {
            this.descriptionEn = descriptionEn;
        }

        public String getCatetgorynameAr() {
            return catetgorynameAr;
        }

        public void setCatetgorynameAr(String catetgorynameAr) {
            this.catetgorynameAr = catetgorynameAr;
        }

        public String getCatetgorynameEn() {
            return catetgorynameEn;
        }

        public void setCatetgorynameEn(String catetgorynameEn) {
            this.catetgorynameEn = catetgorynameEn;
        }

        public int getStorecatid() {
            return storecatid;
        }

        public void setStorecatid(int storecatid) {
            this.storecatid = storecatid;
        }
    }

    public static class StoresDetails implements Serializable {
        @Expose
        @SerializedName("DiscountAmt")
        private int discountamt;
        @Expose
        @SerializedName("PaymentType")
        private String paymenttype;
        @Expose
        @SerializedName("StoreType")
        private String storetype;
        @Expose
        @SerializedName("OrderTypes")
        private String ordertypes;
        @Expose
        @SerializedName("RatingCount")
        private int ratingcount;
        @Expose
        @SerializedName("IsNew")
        private boolean isnew;
        @Expose
        @SerializedName("Popular")
        private boolean popular;
        @Expose
        @SerializedName("IsPromoted")
        private boolean ispromoted;
        @Expose
        @SerializedName("AvgPreparationTime")
        private int avgpreparationtime;
        @Expose
        @SerializedName("DeliveryCharges")
        private int deliverycharges;
        @Expose
        @SerializedName("MinOrderCharges")
        private int minordercharges;
        @Expose
        @SerializedName("Longitude")
        private double longitude;
        @Expose
        @SerializedName("Latitude")
        private double latitude;
        @Expose
        @SerializedName("Rating")
        private String rating;
        @Expose
        @SerializedName("Distance")
        private double distance;
        @Expose
        @SerializedName("StoreImage_En")
        private String storeimageEn;
        @Expose
        @SerializedName("StoreLogo_En")
        private String storelogoEn;
        @Expose
        @SerializedName("StoreStatus")
        private String storestatus;
        @Expose
        @SerializedName("TakeAwayDistance")
        private int takeawaydistance;
        @Expose
        @SerializedName("DeliveryDistance")
        private int deliverydistance;
        @Expose
        @SerializedName("EmailId")
        private String emailid;
        @Expose
        @SerializedName("MobileNo")
        private String mobileno;
        @Expose
        @SerializedName("TelephoneNo")
        private String telephoneno;
        @Expose
        @SerializedName("Address")
        private String address;
        @Expose
        @SerializedName("BranchCode")
        private String branchcode;
        @Expose
        @SerializedName("CurrentDateTime")
        private String currentdatetime;
        @Expose
        @SerializedName("EndDateTime")
        private String enddatetime;
        @Expose
        @SerializedName("StarDateTime")
        private String stardatetime;
        @Expose
        @SerializedName("BranchDescription_Ar")
        private String branchdescriptionAr;
        @Expose
        @SerializedName("BranchDescription_En")
        private String branchdescriptionEn;
        @Expose
        @SerializedName("BrandName_Ar")
        private String brandnameAr;
        @Expose
        @SerializedName("BrandName_En")
        private String brandnameEn;
        @Expose
        @SerializedName("BrandId")
        private int brandid;
        @Expose
        @SerializedName("BranchName_Ar")
        private String branchnameAr;
        @Expose
        @SerializedName("BranchName_En")
        private String branchnameEn;
        @Expose
        @SerializedName("BranchId")
        private int branchid;
        @Expose
        @SerializedName("WeekNo")
        private int weekno;
        @Expose
        @SerializedName("Id")
        private int id;

        public int getDiscountamt() {
            return discountamt;
        }

        public void setDiscountamt(int discountamt) {
            this.discountamt = discountamt;
        }

        public String getPaymenttype() {
            return paymenttype;
        }

        public void setPaymenttype(String paymenttype) {
            this.paymenttype = paymenttype;
        }

        public String getStoretype() {
            return storetype;
        }

        public void setStoretype(String storetype) {
            this.storetype = storetype;
        }

        public String getOrdertypes() {
            return ordertypes;
        }

        public void setOrdertypes(String ordertypes) {
            this.ordertypes = ordertypes;
        }

        public int getRatingcount() {
            return ratingcount;
        }

        public void setRatingcount(int ratingcount) {
            this.ratingcount = ratingcount;
        }

        public boolean isIsnew() {
            return isnew;
        }

        public void setIsnew(boolean isnew) {
            this.isnew = isnew;
        }

        public boolean isPopular() {
            return popular;
        }

        public void setPopular(boolean popular) {
            this.popular = popular;
        }

        public boolean isIspromoted() {
            return ispromoted;
        }

        public void setIspromoted(boolean ispromoted) {
            this.ispromoted = ispromoted;
        }

        public int getAvgpreparationtime() {
            return avgpreparationtime;
        }

        public void setAvgpreparationtime(int avgpreparationtime) {
            this.avgpreparationtime = avgpreparationtime;
        }

        public int getDeliverycharges() {
            return deliverycharges;
        }

        public void setDeliverycharges(int deliverycharges) {
            this.deliverycharges = deliverycharges;
        }

        public int getMinordercharges() {
            return minordercharges;
        }

        public void setMinordercharges(int minordercharges) {
            this.minordercharges = minordercharges;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public String getStoreimageEn() {
            return storeimageEn;
        }

        public void setStoreimageEn(String storeimageEn) {
            this.storeimageEn = storeimageEn;
        }

        public String getStorelogoEn() {
            return storelogoEn;
        }

        public void setStorelogoEn(String storelogoEn) {
            this.storelogoEn = storelogoEn;
        }

        public String getStorestatus() {
            return storestatus;
        }

        public void setStorestatus(String storestatus) {
            this.storestatus = storestatus;
        }

        public int getTakeawaydistance() {
            return takeawaydistance;
        }

        public void setTakeawaydistance(int takeawaydistance) {
            this.takeawaydistance = takeawaydistance;
        }

        public int getDeliverydistance() {
            return deliverydistance;
        }

        public void setDeliverydistance(int deliverydistance) {
            this.deliverydistance = deliverydistance;
        }

        public String getEmailid() {
            return emailid;
        }

        public void setEmailid(String emailid) {
            this.emailid = emailid;
        }

        public String getMobileno() {
            return mobileno;
        }

        public void setMobileno(String mobileno) {
            this.mobileno = mobileno;
        }

        public String getTelephoneno() {
            return telephoneno;
        }

        public void setTelephoneno(String telephoneno) {
            this.telephoneno = telephoneno;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getBranchcode() {
            return branchcode;
        }

        public void setBranchcode(String branchcode) {
            this.branchcode = branchcode;
        }

        public String getCurrentdatetime() {
            return currentdatetime;
        }

        public void setCurrentdatetime(String currentdatetime) {
            this.currentdatetime = currentdatetime;
        }

        public String getEnddatetime() {
            return enddatetime;
        }

        public void setEnddatetime(String enddatetime) {
            this.enddatetime = enddatetime;
        }

        public String getStardatetime() {
            return stardatetime;
        }

        public void setStardatetime(String stardatetime) {
            this.stardatetime = stardatetime;
        }

        public String getBranchdescriptionAr() {
            return branchdescriptionAr;
        }

        public void setBranchdescriptionAr(String branchdescriptionAr) {
            this.branchdescriptionAr = branchdescriptionAr;
        }

        public String getBranchdescriptionEn() {
            return branchdescriptionEn;
        }

        public void setBranchdescriptionEn(String branchdescriptionEn) {
            this.branchdescriptionEn = branchdescriptionEn;
        }

        public String getBrandnameAr() {
            return brandnameAr;
        }

        public void setBrandnameAr(String brandnameAr) {
            this.brandnameAr = brandnameAr;
        }

        public String getBrandnameEn() {
            return brandnameEn;
        }

        public void setBrandnameEn(String brandnameEn) {
            this.brandnameEn = brandnameEn;
        }

        public int getBrandid() {
            return brandid;
        }

        public void setBrandid(int brandid) {
            this.brandid = brandid;
        }

        public String getBranchnameAr() {
            return branchnameAr;
        }

        public void setBranchnameAr(String branchnameAr) {
            this.branchnameAr = branchnameAr;
        }

        public String getBranchnameEn() {
            return branchnameEn;
        }

        public void setBranchnameEn(String branchnameEn) {
            this.branchnameEn = branchnameEn;
        }

        public int getBranchid() {
            return branchid;
        }

        public void setBranchid(int branchid) {
            this.branchid = branchid;
        }

        public int getWeekno() {
            return weekno;
        }

        public void setWeekno(int weekno) {
            this.weekno = weekno;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    /*Comparator for sorting the list by roll no*/
    public static Comparator<StoresList.StoresDetails> distanceSort = new Comparator<StoresList.StoresDetails>() {
        public int compare(StoresList.StoresDetails s1, StoresList.StoresDetails s2) {
            Double rollno1 = (s1.getDistance());
            Double rollno2 = (s2.getDistance());
            return Double.compare(rollno1,rollno2);
        }};
}
